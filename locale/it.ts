<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutView</name>
    <message>
        <location filename="src/aboutview.cpp" line="32"/>
        <source>There&apos;s life outside the browser!</source>
        <translation>C&apos;è vita fuori del browser!</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="33"/>
        <source>Version %1</source>
        <translation>Versione %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="38"/>
        <source>Licensed to: %1</source>
        <translation>Licenziato a: %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="42"/>
        <source>%1 is Free Software but its development takes precious time.</source>
        <translation>%1 è Software Libero ma il suo sviluppo richiede tempo prezioso.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="43"/>
        <source>Please &lt;a href=&apos;%1&apos;&gt;donate&lt;/a&gt; to support the continued development of %2.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Fai una donazione&lt;/a&gt; per aiutare lo sviluppo di %2.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="47"/>
        <source>You may want to try my other apps as well:</source>
        <translation>Prova anche le mie altre applicazioni:</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="50"/>
        <source>%1, a YouTube music player</source>
        <translation>%1, un player musicale per YouTube</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="54"/>
        <source>%1, a music player</source>
        <translation>%1, un player musicale</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="60"/>
        <source>Translate %1 to your native language using %2</source>
        <translation>Traduci %1 nella tua lingua usando %2</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="65"/>
        <source>Icon designed by %1.</source>
        <translation>Icona disegnata da %1.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="69"/>
        <source>Released under the &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</source>
        <translation>Rilasciato sotto licenza &lt;a href=&quot;%1&quot;&gt;GNU General Public License&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="84"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="17"/>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="19"/>
        <source>What you always wanted to know about %1 and never dared to ask</source>
        <translation>Quello che hai sempre voluto sapere su %1 e non hai mai osato chiedere</translation>
    </message>
</context>
<context>
    <name>ActivationDialog</name>
    <message>
        <location filename="local/src/activationdialog.cpp" line="17"/>
        <source>Enter your License Details</source>
        <translation>Inserisci i dati della tua licenza:</translation>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="29"/>
        <source>&amp;Email:</source>
        <translation>&amp;Email:</translation>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="35"/>
        <source>&amp;Code:</source>
        <translation>&amp;Codice:</translation>
    </message>
</context>
<context>
    <name>ActivationView</name>
    <message>
        <location filename="local/src/activationview.cpp" line="47"/>
        <source>Please license %1</source>
        <translation>Compra %1</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="51"/>
        <source>This demo has expired.</source>
        <translation>Questa demo è scaduta.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="53"/>
        <source>The full version allows you to download videos longer than %1 minutes and to watch videos without interruptions.</source>
        <translation>La versione completa ti permette di scaricare video più lunghi di %1 minuti e di guardare i video senza interruzioni.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="55"/>
        <source>Without a license, the application will expire in %1 days.</source>
        <translation>Senza una licenza, l&apos;applicazione scadrà in %1 giorni.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="57"/>
        <source>By purchasing the full version, you will also support the hard work I put into creating %1.</source>
        <translation>Acquistando la versione completa, supporterai anche il lavoro che ho fatto per creare %1.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="70"/>
        <source>Use Demo</source>
        <translation>Usa la demo</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="77"/>
        <source>Enter License</source>
        <translation>Inserisci la tua licenza</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="85"/>
        <source>Buy License</source>
        <translation>Compra una licenza</translation>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="56"/>
        <source>Clear</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="src/downloaditem.cpp" line="322"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="325"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="328"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="337"/>
        <source>bytes/sec</source>
        <translation>bytes/sec</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="340"/>
        <source>KB/sec</source>
        <translation>KB/sec</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="343"/>
        <source>MB/sec</source>
        <translation>MB/sec</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="349"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="352"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="355"/>
        <source>%4 %5 remaining</source>
        <translation>%4 %5 al termine</translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="55"/>
        <source>This is just the demo version of %1.</source>
        <translation>Questa è solo la versione demo di %1.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="57"/>
        <source>It can only download videos shorter than %1 minutes so you can test the download functionality.</source>
        <translation>Puoi scaricare solo video più corti di %1 minuti, così puoi testare la funzionalità dei download.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="63"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="64"/>
        <source>Get the full version</source>
        <translation>Compra la versione completa</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="150"/>
        <source>%1 downloaded in %2</source>
        <translation>%1 scaricato in %2</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="153"/>
        <source>Download finished</source>
        <translation>Download completato</translation>
    </message>
    <message numerus="yes">
        <location filename="src/downloadmanager.cpp" line="159"/>
        <source>%n Download(s)</source>
        <translation><numerusform>1 download</numerusform><numerusform>%n download</numerusform></translation>
    </message>
</context>
<context>
    <name>DownloadSettings</name>
    <message>
        <location filename="src/downloadsettings.cpp" line="15"/>
        <source>Change location...</source>
        <translation>Cambia destinazione...</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="47"/>
        <source>Choose the download location</source>
        <translation>Scegli la destinazione dei download</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="59"/>
        <source>Download location changed.</source>
        <translation>La destinazione dei download è cambiata.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="61"/>
        <source>Current downloads will still go in the previous location.</source>
        <translation>I download già in corso andranno comunque nella destinazione precedente.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="72"/>
        <source>Downloading to: %1</source>
        <translation>Scarica in: %1</translation>
    </message>
</context>
<context>
    <name>DownloadView</name>
    <message>
        <location filename="src/downloadview.cpp" line="18"/>
        <location filename="src/downloadview.h" line="22"/>
        <source>Downloads</source>
        <translation>Download</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="60"/>
        <source>Downloading update...</source>
        <translation>Download dell&apos;aggiornamento in corso...</translation>
    </message>
</context>
<context>
    <name>GlobalShortcuts</name>
    <message>
        <location filename="src/globalshortcuts.cpp" line="16"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="17"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="18"/>
        <source>Play/Pause</source>
        <translation>Play/Pausa</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="19"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="20"/>
        <source>Stop playing after current track</source>
        <translation>Ferma al termine del video corrente</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="21"/>
        <source>Next track</source>
        <translation>Prossima traccia</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="22"/>
        <source>Previous track</source>
        <translation>Traccia precedente</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="23"/>
        <source>Increase volume</source>
        <translation>Aumenta il volume</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="24"/>
        <source>Decrease volume</source>
        <translation>Diminuisci il volume</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="25"/>
        <source>Mute</source>
        <translation>Senza audio</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="26"/>
        <source>Seek forward</source>
        <translation>Salta in avanti</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="27"/>
        <source>Seek backward</source>
        <translation>Salta indietro</translation>
    </message>
</context>
<context>
    <name>HomeView</name>
    <message>
        <location filename="src/homeview.cpp" line="33"/>
        <source>Search</source>
        <translation>Ricerca</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="35"/>
        <source>Find videos and channels by keyword</source>
        <translation>Trova video e canali per parola chiave</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="40"/>
        <source>Browse</source>
        <translation>Sfoglia</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="42"/>
        <source>Browse videos by category</source>
        <translation>Sfoglia i video per categoria</translation>
    </message>
    <message>
        <location filename="src/homeview.h" line="22"/>
        <source>Make yourself comfortable</source>
        <translation>Mettiti comodo</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="src/loadingwidget.cpp" line="90"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.cpp" line="213"/>
        <source>&amp;Stop</source>
        <translation>&amp;Ferma</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="214"/>
        <source>Stop playback and go back to the search view</source>
        <translation>Ferma il video e torna alla ricerca</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="232"/>
        <source>S&amp;kip</source>
        <translation>&amp;Salta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="233"/>
        <source>Skip to the next video</source>
        <translation>Salta al prossimo video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="239"/>
        <location filename="src/mainwindow.cpp" line="989"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pausa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="240"/>
        <location filename="src/mainwindow.cpp" line="990"/>
        <source>Pause playback</source>
        <translation>Metti in pausa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="246"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Schermo intero</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Go full screen</source>
        <translation>Vai in modalità schermo intero</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="263"/>
        <source>Hide the playlist and the toolbar</source>
        <translation>Nascondi la playlist e la barra degli strumenti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="276"/>
        <source>Go to the YouTube video page and pause playback</source>
        <translation>Vai su YouTube e metti in pausa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="283"/>
        <source>Copy the current video YouTube link to the clipboard</source>
        <translation>Copia negli appunti il link a YouTube per il video corrente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="290"/>
        <source>Copy the current video stream URL to the clipboard</source>
        <translation>Copia negli appunti la URL dello stream per il video corrente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="297"/>
        <source>Find other video parts hopefully in the right order</source>
        <translation>Trova le parti mancanti di questo video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="303"/>
        <source>&amp;Remove</source>
        <translation>&amp;Elimina</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="304"/>
        <source>Remove the selected videos from the playlist</source>
        <translation>Elimina i video selezionati dalla playlist</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="310"/>
        <source>Move &amp;Up</source>
        <translation>Sposta &amp;sopra</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="311"/>
        <source>Move up the selected videos in the playlist</source>
        <translation>Sposta video selezionati verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="317"/>
        <source>Move &amp;Down</source>
        <translation>Sposta so&amp;tto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="318"/>
        <source>Move down the selected videos in the playlist</source>
        <translation>Sposta i video selezionati verso il basso</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="329"/>
        <source>Clear the search history. Cannot be undone.</source>
        <translation>Cancella le ultime ricerche. L&apos;azione non potrà essere annullata.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="337"/>
        <source>Bye</source>
        <translation>Ciao</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="341"/>
        <source>&amp;Website</source>
        <translation>Sito &amp;web</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="343"/>
        <source>%1 on the Web</source>
        <translation>%1 sul Web</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="349"/>
        <source>Please support the continued development of %1</source>
        <translation>Supporta lo sviluppo di %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="354"/>
        <source>&amp;About</source>
        <translation>&amp;Informazioni</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="356"/>
        <source>Info about %1</source>
        <translation>Informazioni su %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="364"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="383"/>
        <source>Mute volume</source>
        <translation>Disattiva l&apos;audio</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="412"/>
        <location filename="src/mainwindow.cpp" line="1365"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Download</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="413"/>
        <source>Show details about video downloads</source>
        <translation>Mostra informazioni sui download</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="421"/>
        <source>&amp;Download</source>
        <translation>&amp;Download</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="422"/>
        <source>Download the current video</source>
        <translation>Scarica il video corrente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="441"/>
        <source>Share the current video using %1</source>
        <translation>Condividi il video corrente usando %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="458"/>
        <source>&amp;Email</source>
        <translation>&amp;Email</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="459"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="463"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="473"/>
        <source>&amp;Float on Top</source>
        <translation>&amp;Fluttua in alto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="478"/>
        <source>&amp;Stop After This Video</source>
        <translation>&amp;Ferma dopo questo video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="485"/>
        <source>&amp;Report an Issue...</source>
        <translation>&amp;Segnala un problema...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="489"/>
        <source>&amp;Refine Search...</source>
        <translation>&amp;Raffina la ricerca...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="501"/>
        <source>More...</source>
        <translation>Altro...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="504"/>
        <source>&amp;Related Videos</source>
        <translation>Video &amp;correlati</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="506"/>
        <source>Watch videos related to the current one</source>
        <translation>Guarda i video correlati a quello corrente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="544"/>
        <source>&amp;Application</source>
        <translation>&amp;Applicazione</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="515"/>
        <source>Buy %1...</source>
        <translation>Compra %1...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="558"/>
        <source>&amp;Playback</source>
        <translation>&amp;Riproduzione</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="572"/>
        <source>&amp;Playlist</source>
        <translation>&amp;Playlist</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="581"/>
        <source>&amp;Video</source>
        <translation>&amp;Video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="594"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="601"/>
        <source>&amp;Share</source>
        <translation>&amp;Condividi</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="690"/>
        <source>Press %1 to raise the volume, %2 to lower it</source>
        <translation>Premi %1 per alzare il volume, %2 per abbassarlo</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="889"/>
        <location filename="src/mainwindow.cpp" line="895"/>
        <source>Opening %1</source>
        <translation>Apertura di %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="936"/>
        <source>Do you want to exit %1 with a download in progress?</source>
        <translation>Vuoi uscire da %1 con un download in corso?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="937"/>
        <source>If you close %1 now, this download will be cancelled.</source>
        <translation>Se chiudi %1 adesso, i download verranno annullati.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="942"/>
        <source>Close and cancel download</source>
        <translation>Chiudi e annulla i download</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="943"/>
        <source>Wait for download to finish</source>
        <translation>Aspetta che i download siano completi</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1102"/>
        <source>Leave &amp;Full Screen</source>
        <translation>Lascia lo &amp;schermo intero</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1475"/>
        <source>%1 version %2 is now available.</source>
        <translation>E&apos; disponibile %1 versione %2</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1479"/>
        <source>Remind me later</source>
        <translation>Ricordamelo più tardi</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1480"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="982"/>
        <source>Error: %1</source>
        <translation>Errore: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="222"/>
        <source>P&amp;revious</source>
        <translation>P&amp;recedente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="223"/>
        <source>Go back to the previous track</source>
        <translation>Torna alla traccia precedente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="262"/>
        <source>&amp;Compact Mode</source>
        <translation>Modalità &amp;compatta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="275"/>
        <source>Open the &amp;YouTube Page</source>
        <translation>Apri la pagina di &amp;YouTube</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="282"/>
        <source>Copy the YouTube &amp;Link</source>
        <translation>Copia il &amp;link a YouTube</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="289"/>
        <source>Copy the Video Stream &amp;URL</source>
        <translation>Copia la &amp;URL dello stream video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="296"/>
        <source>Find Video &amp;Parts</source>
        <translation>Trova le &amp;parti del video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>&amp;Clear Recent Searches</source>
        <translation>&amp;Cancella le ultime ricerche</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="348"/>
        <source>Make a &amp;Donation</source>
        <translation>Fai una &amp;donazione</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="405"/>
        <source>&amp;Manually Start Playing</source>
        <translation>Riproduzione &amp;manuale</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="406"/>
        <source>Manually start playing videos</source>
        <translation>Inizia a riprodurre i video manualmente</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="728"/>
        <source>Choose your content location</source>
        <translation>Scegli la località dei contenuti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1002"/>
        <source>&amp;Play</source>
        <translation>&amp;Play</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1003"/>
        <source>Resume playback</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1236"/>
        <source>Remaining time: %1</source>
        <translation>Tempo rimanente: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1294"/>
        <source>Volume at %1%</source>
        <translation>Volume al %1%</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1300"/>
        <source>Volume is muted</source>
        <translation>Audio disattivato</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1303"/>
        <source>Volume is unmuted</source>
        <translation>Audio attivato</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1310"/>
        <source>Maximum video definition set to %1</source>
        <translation>La definizione video massima è impostata a %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1351"/>
        <source>Your privacy is now safe</source>
        <translation>La tua privacy è al sicuro</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1366"/>
        <source>Downloads complete</source>
        <translation>I download sono completi</translation>
    </message>
</context>
<context>
    <name>MediaView</name>
    <message>
        <location filename="src/mediaview.cpp" line="553"/>
        <source>You can now paste the YouTube link into another application</source>
        <translation>Ora puoi incollare il link a YouTube in un&apos;altra applicazione</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="561"/>
        <source>You can now paste the video stream URL into another application</source>
        <translation>Ora puoi incollare la URL dello stream in un&apos;altra applicazione</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="562"/>
        <source>The link will be valid only for a limited time.</source>
        <translation>Il link rimarrà valido per un periodo di tempo limitato.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="636"/>
        <source>This is just the demo version of %1.</source>
        <translation>Questa è solo la versione demo di %1.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="637"/>
        <source>It allows you to test the application and see if it works for you.</source>
        <translation>Ti permette di testare l&apos;applicazione e verificare che funzioni sul tuo computer.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="667"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="756"/>
        <source>of</source>
        <comment>Used in video parts, as in '2 of 3'</comment>
        <translation>di</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="768"/>
        <source>part</source>
        <comment>This is for video parts, as in 'Cool video - part 1'</comment>
        <translation>parte</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="770"/>
        <source>episode</source>
        <comment>This is for video parts, as in 'Cool series - episode 1'</comment>
        <translation>episodio</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="847"/>
        <source>Sent from %1</source>
        <translation>Inviato da %1</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="644"/>
        <source>Get the full version</source>
        <translation>Compra la versione completa</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="681"/>
        <source>Downloading %1</source>
        <translation>Scarica in: %1</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="21"/>
        <source>A new version of %1 is available!</source>
        <translation>Una nuova versione di %1 è disponibile!</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="28"/>
        <source>%1 %2 is now available. You have %3.</source>
        <translation>%1 %2 è ora disponibile. Tu hai la %3.</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="33"/>
        <source>Would you like to download it now?</source>
        <translation>Vuoi scaricare ora?</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="39"/>
        <source>Skip This Version</source>
        <translation>Salta questa versione</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="43"/>
        <source>Remind Me Later</source>
        <translation>Ricordamelo più tardi</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="47"/>
        <source>Install Update</source>
        <translation>Installa aggiornamento</translation>
    </message>
</context>
<context>
    <name>PasteLineEdit</name>
    <message>
        <location filename="local/src/pastelineedit.cpp" line="6"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="159"/>
        <source>%1 views</source>
        <translation>%1 visualizzazioni</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="285"/>
        <source>%1 of %2 (%3) — %4</source>
        <translation>%1 di %2 (%3) — %4</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="292"/>
        <source>Preparing</source>
        <translation>In preparazione</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="294"/>
        <source>Failed</source>
        <translation>Fallito</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="296"/>
        <source>Completed</source>
        <translation>Completato</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="298"/>
        <source>Stopped</source>
        <translation>Fermo</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="334"/>
        <source>Stop downloading</source>
        <translation>Interrompi il download</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="344"/>
        <source>Show in %1</source>
        <translation>Mostra nel %1</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="346"/>
        <source>Open parent folder</source>
        <translation>Mostra nella cartella</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="355"/>
        <source>Restart downloading</source>
        <translation>Prova a scaricare di nuovo</translation>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="src/playlistmodel.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="53"/>
        <source>Show %1 More</source>
        <translation>Mostra altri %1</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="54"/>
        <source>No videos</source>
        <translation>Nessun video</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="55"/>
        <source>No more videos</source>
        <translation>Nessun altro video</translation>
    </message>
</context>
<context>
    <name>RefineSearchWidget</name>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="31"/>
        <source>Sort by</source>
        <translation>Ordina per</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="35"/>
        <source>Relevance</source>
        <translation>Pertinenza</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="36"/>
        <location filename="src/refinesearchwidget.cpp" line="52"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="37"/>
        <source>View Count</source>
        <translation>Visualizzazioni</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="38"/>
        <source>Rating</source>
        <translation>Voto</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="56"/>
        <source>Anytime</source>
        <translation>Qualsiasi</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="57"/>
        <source>Today</source>
        <translation>Oggi</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="58"/>
        <source>7 Days</source>
        <translation>7 giorni</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="59"/>
        <source>30 Days</source>
        <translation>30 giorni</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="73"/>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="77"/>
        <location filename="src/refinesearchwidget.cpp" line="104"/>
        <source>All</source>
        <translation>Tutti</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="78"/>
        <source>Short</source>
        <translation>Breve</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="79"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="80"/>
        <source>Long</source>
        <translation>Lunga</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="83"/>
        <source>Less than 4 minutes</source>
        <translation>Meno di 4 minuti</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="84"/>
        <source>Between 4 and 20 minutes</source>
        <translation>Tra i 4 e i 20 minuti</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="85"/>
        <source>Longer than 20 minutes</source>
        <translation>Oltre i 20 minuti</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="100"/>
        <source>Quality</source>
        <translation>Qualità</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="105"/>
        <source>High Definition</source>
        <translation>Alta definizione</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="108"/>
        <source>720p or higher</source>
        <translation>720p o maggiore</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="122"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>RegionsView</name>
    <message>
        <location filename="src/regionsview.cpp" line="19"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="177"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="src/searchview.cpp" line="71"/>
        <source>Welcome to &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</source>
        <translation>Benvenuto su &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="50"/>
        <source>Get the full version</source>
        <translation>Compra la versione completa</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="85"/>
        <source>Enter</source>
        <extracomment>&quot;Enter&quot;, as in &quot;type&quot;. The whole phrase says: &quot;Enter a keyword to start watching videos&quot;</extracomment>
        <translation>Scrivi</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="90"/>
        <source>a keyword</source>
        <translation>una parola chiave</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="91"/>
        <source>a channel</source>
        <translation>un canale</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="96"/>
        <source>to start watching videos.</source>
        <translation>per iniziare a guardare i video.</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="120"/>
        <source>Watch</source>
        <translation>Guarda</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="138"/>
        <source>Recent keywords</source>
        <translation>Ultime ricerche</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="151"/>
        <source>Recent channels</source>
        <translation>Ultimi canali</translation>
    </message>
</context>
<context>
    <name>SidebarHeader</name>
    <message>
        <location filename="src/sidebarheader.cpp" line="16"/>
        <location filename="src/sidebarheader.cpp" line="23"/>
        <source>&amp;Back</source>
        <translation>&amp;Indietro</translation>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="57"/>
        <source>Forward to %1</source>
        <translation>Avanza a %1</translation>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="70"/>
        <source>Back to %1</source>
        <translation>Torna a %1</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <location filename="src/sidebarwidget.cpp" line="52"/>
        <source>Refine Search</source>
        <translation>Raffina la ricerca</translation>
    </message>
    <message>
        <location filename="src/sidebarwidget.cpp" line="148"/>
        <source>Did you mean: %1</source>
        <translation>Forse cercavi: %1</translation>
    </message>
</context>
<context>
    <name>StandardFeedsView</name>
    <message>
        <location filename="src/standardfeedsview.cpp" line="85"/>
        <source>Most Popular</source>
        <translation>Più popolari</translation>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="86"/>
        <source>Featured</source>
        <translation>In primo piano</translation>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="87"/>
        <source>Most Shared</source>
        <translation>Più condivisi</translation>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="88"/>
        <source>Most Discussed</source>
        <translation>Più discussi</translation>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="89"/>
        <source>Top Rated</source>
        <translation>Più votati</translation>
    </message>
</context>
<context>
    <name>Video</name>
    <message>
        <location filename="src/video.cpp" line="213"/>
        <source>Cannot get video stream for %1</source>
        <translation>Impossibile ottenere il flusso video per %1</translation>
    </message>
    <message>
        <location filename="src/video.cpp" line="231"/>
        <source>Network error: %1 for %2</source>
        <translation>Errore di rete: %1 per %2</translation>
    </message>
</context>
<context>
    <name>YTRegions</name>
    <message>
        <location filename="src/ytregions.cpp" line="8"/>
        <source>Algeria</source>
        <translation>Algeria</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="9"/>
        <source>Argentina</source>
        <translation>Argentina</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="10"/>
        <source>Australia</source>
        <translation>Australia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="11"/>
        <source>Belgium</source>
        <translation>Belgio</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="12"/>
        <source>Brazil</source>
        <translation>Brasile</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="13"/>
        <source>Canada</source>
        <translation>Canada</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="14"/>
        <source>Chile</source>
        <translation>Cile</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="15"/>
        <source>Colombia</source>
        <translation>Colombia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="16"/>
        <source>Czech Republic</source>
        <translation>Repubblica Ceca</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="17"/>
        <source>Egypt</source>
        <translation>Egitto</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="18"/>
        <source>France</source>
        <translation>Francia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="19"/>
        <source>Germany</source>
        <translation>Germania</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="20"/>
        <source>Ghana</source>
        <translation>Ghana</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="21"/>
        <source>Greece</source>
        <translation>Grecia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="22"/>
        <source>Hong Kong</source>
        <translation>Hong Kong</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="23"/>
        <source>Hungary</source>
        <translation>Ungheria</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="24"/>
        <source>India</source>
        <translation>India</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="25"/>
        <source>Indonesia</source>
        <translation>Indonesia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="26"/>
        <source>Ireland</source>
        <translation>Irlanda</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="27"/>
        <source>Israel</source>
        <translation>Israele</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="28"/>
        <source>Italy</source>
        <translation>Italia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="29"/>
        <source>Japan</source>
        <translation>Giappone</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="30"/>
        <source>Jordan</source>
        <translation>Giordania</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="31"/>
        <source>Kenya</source>
        <translation>Kenya</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="32"/>
        <source>Malaysia</source>
        <translation>Malaysia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="33"/>
        <source>Mexico</source>
        <translation>Messico</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="34"/>
        <source>Morocco</source>
        <translation>Marocco</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="35"/>
        <source>Netherlands</source>
        <translation>Olanda</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="36"/>
        <source>New Zealand</source>
        <translation>Nuova Zelanda</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="37"/>
        <source>Nigeria</source>
        <translation>Nigeria</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="38"/>
        <source>Peru</source>
        <translation>Perù</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="39"/>
        <source>Philippines</source>
        <translation>Filippine</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="40"/>
        <source>Poland</source>
        <translation>Polonia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="41"/>
        <source>Russia</source>
        <translation>Russia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="42"/>
        <source>Saudi Arabia</source>
        <translation>Arabia Saudita</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="43"/>
        <source>Singapore</source>
        <translation>Singapore</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="44"/>
        <source>South Africa</source>
        <translation>Sudafrica</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="45"/>
        <source>South Korea</source>
        <translation>Corea del Sud</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="46"/>
        <source>Spain</source>
        <translation>Spagna</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="47"/>
        <source>Sweden</source>
        <translation>Svezia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="48"/>
        <source>Taiwan</source>
        <translation>Taiwan</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="49"/>
        <source>Tunisia</source>
        <translation>Tunisia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="50"/>
        <source>Turkey</source>
        <translation>Turchia</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="51"/>
        <source>Uganda</source>
        <translation>Uganda</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="52"/>
        <source>United Arab Emirates</source>
        <translation>Emirati Arabi Uniti</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="53"/>
        <source>United Kingdom</source>
        <translation>Regno Unito</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="54"/>
        <source>Yemen</source>
        <translation>Yemen</translation>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="128"/>
        <source>Worldwide</source>
        <translation>Tutto il mondo</translation>
    </message>
</context>
</TS>