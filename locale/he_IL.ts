<?xml version="1.0" ?><!DOCTYPE TS><TS language="he_IL" version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutView</name>
    <message>
        <location filename="src/aboutview.cpp" line="32"/>
        <source>There&apos;s life outside the browser!</source>
        <translation>יש חיים מחוץ לדפדפן!</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="33"/>
        <source>Version %1</source>
        <translation>גרסה %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="38"/>
        <source>Licensed to: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="42"/>
        <source>%1 is Free Software but its development takes precious time.</source>
        <translation>%1 הנה תכנה חופשית אך פיתוחה לוקח המון זמן יקר.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="43"/>
        <source>Please &lt;a href=&apos;%1&apos;&gt;donate&lt;/a&gt; to support the continued development of %2.</source>
        <translation>אנא &lt;a href=&apos;%1&apos;&gt;תרמו&lt;/a&gt; כדי לתמוך בהמשך הפיתוח של %2.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="47"/>
        <source>You may want to try my other apps as well:</source>
        <translation>מומלץ לנסות להשתמש בשאר היישומים שלי:</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="50"/>
        <source>%1, a YouTube music player</source>
        <translation>%1, נגן מוזיקה מיוטיוב</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="54"/>
        <source>%1, a music player</source>
        <translation>%1, נגן מוזיקה</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="60"/>
        <source>Translate %1 to your native language using %2</source>
        <translation>ניתן לתרגם את %1 לשפת אמך באמצעות %2</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="65"/>
        <source>Icon designed by %1.</source>
        <translation>הסמל עוצב על ידי %1.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="69"/>
        <source>Released under the &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</source>
        <translation>התכנה שוחררה תחת תנאי &lt;a href=&apos;%1&apos;&gt;הרישיון הציבורי הכללי של GNU&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="84"/>
        <source>&amp;Close</source>
        <translation>&amp;סגירה</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="17"/>
        <source>About</source>
        <translation>על אודות</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="19"/>
        <source>What you always wanted to know about %1 and never dared to ask</source>
        <translation>מה שתמיד רצית לדעת על %1 ולא העזת לשאול</translation>
    </message>
</context>
<context>
    <name>ActivationDialog</name>
    <message>
        <location filename="local/src/activationdialog.cpp" line="17"/>
        <source>Enter your License Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="29"/>
        <source>&amp;Email:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="35"/>
        <source>&amp;Code:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ActivationView</name>
    <message>
        <location filename="local/src/activationview.cpp" line="47"/>
        <source>Please license %1</source>
        <translation>נא לרכוש את %1</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="51"/>
        <source>This demo has expired.</source>
        <translation>גרסת הדגמה זו פגה.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="53"/>
        <source>The full version allows you to download videos longer than %1 minutes and to watch videos without interruptions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="55"/>
        <source>Without a license, the application will expire in %1 days.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="57"/>
        <source>By purchasing the full version, you will also support the hard work I put into creating %1.</source>
        <translation>רכישת הגרסה המלאה מהווה תמיכה בעבודה הקשה שאני משקיע ביצירת %1.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="70"/>
        <source>Use Demo</source>
        <translation>שימוש בהדגמה</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="77"/>
        <source>Enter License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="85"/>
        <source>Buy License</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="56"/>
        <source>Clear</source>
        <translation>מחיקה</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="src/downloaditem.cpp" line="322"/>
        <source>bytes</source>
        <translation>בתים</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="325"/>
        <source>KB</source>
        <translation>ק״ב</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="328"/>
        <source>MB</source>
        <translation>מ״ב</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="337"/>
        <source>bytes/sec</source>
        <translation>בתים/שנייה</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="340"/>
        <source>KB/sec</source>
        <translation>ק״ב/שנייה</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="343"/>
        <source>MB/sec</source>
        <translation>מ״ב/שנייה</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="349"/>
        <source>seconds</source>
        <translation>שניות</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="352"/>
        <source>minutes</source>
        <translation>דקות</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="355"/>
        <source>%4 %5 remaining</source>
        <translation>%4 %5 נותרו</translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="55"/>
        <source>This is just the demo version of %1.</source>
        <translation>זוהי רק גרסת ההדגמה של %1.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="57"/>
        <source>It can only download videos shorter than %1 minutes so you can test the download functionality.</source>
        <translation>באמצעות גרסה זו ניתן להוריד קטעי וידאו שאורכם אינו עולה על %1 דקות כדי שתהיה באפשרותך לבחור את אפשרות ההורדה.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="63"/>
        <source>Continue</source>
        <translation>המשך</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="64"/>
        <source>Get the full version</source>
        <translation>קבלת הגרסה המלאה</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="150"/>
        <source>%1 downloaded in %2</source>
        <translation>%1 התקבל במהירות של %2</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="153"/>
        <source>Download finished</source>
        <translation>ההורדה הושלמה</translation>
    </message>
    <message numerus="yes">
        <location filename="src/downloadmanager.cpp" line="159"/>
        <source>%n Download(s)</source>
        <translation><numerusform>הורדה אחת</numerusform><numerusform>%n הורדות</numerusform></translation>
    </message>
</context>
<context>
    <name>DownloadSettings</name>
    <message>
        <location filename="src/downloadsettings.cpp" line="15"/>
        <source>Change location...</source>
        <translation>שינוי המיקום...</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="47"/>
        <source>Choose the download location</source>
        <translation>נא לבחור את מיקום ההורדה</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="59"/>
        <source>Download location changed.</source>
        <translation>מיקום ההורדה השתנה.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="61"/>
        <source>Current downloads will still go in the previous location.</source>
        <translation>ההורדה הנוכחית עדיין תישמר במיקום הקודם.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="72"/>
        <source>Downloading to: %1</source>
        <translation>מתבצעת הורדה אל: %1</translation>
    </message>
</context>
<context>
    <name>DownloadView</name>
    <message>
        <location filename="src/downloadview.cpp" line="18"/>
        <location filename="src/downloadview.h" line="22"/>
        <source>Downloads</source>
        <translation>הורדות</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="60"/>
        <source>Downloading update...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GlobalShortcuts</name>
    <message>
        <location filename="src/globalshortcuts.cpp" line="16"/>
        <source>Play</source>
        <translation>נגינה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="17"/>
        <source>Pause</source>
        <translation>השהיה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="18"/>
        <source>Play/Pause</source>
        <translation>נגינה/השהיה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="19"/>
        <source>Stop</source>
        <translation>עצירה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="20"/>
        <source>Stop playing after current track</source>
        <translation>עצירת הנגינה לאחר הרצועה הנוכחית</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="21"/>
        <source>Next track</source>
        <translation>הרצועה הבאה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="22"/>
        <source>Previous track</source>
        <translation>הרצועה קודמת</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="23"/>
        <source>Increase volume</source>
        <translation>הגברת עצמת השמע</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="24"/>
        <source>Decrease volume</source>
        <translation>הנמכת עצמת השמע</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="25"/>
        <source>Mute</source>
        <translation>השתקה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="26"/>
        <source>Seek forward</source>
        <translation>סריקה קדימה</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="27"/>
        <source>Seek backward</source>
        <translation>סריקה אחורה</translation>
    </message>
</context>
<context>
    <name>HomeView</name>
    <message>
        <location filename="src/homeview.cpp" line="33"/>
        <source>Search</source>
        <translation>חיפוש</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="35"/>
        <source>Find videos and channels by keyword</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="40"/>
        <source>Browse</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="42"/>
        <source>Browse videos by category</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.h" line="22"/>
        <source>Make yourself comfortable</source>
        <translation>שבו בניחותא</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="src/loadingwidget.cpp" line="90"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.cpp" line="213"/>
        <source>&amp;Stop</source>
        <translation>&amp;עצירה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="214"/>
        <source>Stop playback and go back to the search view</source>
        <translation>עצירת הנגינה וחזרה אל תצוגת החיפוש</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="232"/>
        <source>S&amp;kip</source>
        <translation>&amp;דילוג</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="233"/>
        <source>Skip to the next video</source>
        <translation>דילוג אל הסרטון הבא</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="239"/>
        <location filename="src/mainwindow.cpp" line="989"/>
        <source>&amp;Pause</source>
        <translation>ה&amp;שהיה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="240"/>
        <location filename="src/mainwindow.cpp" line="990"/>
        <source>Pause playback</source>
        <translation>השהיית הנגינה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="246"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;מסך מלא</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Go full screen</source>
        <translation>מעבר למסך מלא</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="263"/>
        <source>Hide the playlist and the toolbar</source>
        <translation>הסתרת רשימת ההשמעה וסרגל הכלים</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="276"/>
        <source>Go to the YouTube video page and pause playback</source>
        <translation>מעבר לעמוד הווידאו ב־YouTube והשהיית הנגינה.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="283"/>
        <source>Copy the current video YouTube link to the clipboard</source>
        <translation>העתקת הקישור אל הווידאו הנוכחי אל לוח הגזירים</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="290"/>
        <source>Copy the current video stream URL to the clipboard</source>
        <translation>העתקת כתובת תזרים הווידאו אל לוח הגזירים</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="297"/>
        <source>Find other video parts hopefully in the right order</source>
        <translation>חיפוש חלקים אחרים בתקווה שהם בסדר הנכון</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="303"/>
        <source>&amp;Remove</source>
        <translation>ה&amp;סרה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="304"/>
        <source>Remove the selected videos from the playlist</source>
        <translation>הסרת הסרטונים הנבחרים מרשימת ההשמעה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="310"/>
        <source>Move &amp;Up</source>
        <translation>הזזה מ&amp;מעלה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="311"/>
        <source>Move up the selected videos in the playlist</source>
        <translation>הזזת הסרטונים הנבחרים במעלה רשימת ההשמעה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="317"/>
        <source>Move &amp;Down</source>
        <translation>הזזה מ&amp;טה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="318"/>
        <source>Move down the selected videos in the playlist</source>
        <translation>הזזת הסרטונים הנבחרים במורד רשימת ההשמעה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="329"/>
        <source>Clear the search history. Cannot be undone.</source>
        <translation>מחיקת היסטוריית החיפוש. לא ניתן לבטל</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>י&amp;ציאה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="337"/>
        <source>Bye</source>
        <translation>להתראות</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="341"/>
        <source>&amp;Website</source>
        <translation>&amp;אתר האינטרנט</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="343"/>
        <source>%1 on the Web</source>
        <translation>%1 באינטרנט</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="349"/>
        <source>Please support the continued development of %1</source>
        <translation>אנא תמכו בהמשך הפיתוח של %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="354"/>
        <source>&amp;About</source>
        <translation>&amp;על אודות</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="356"/>
        <source>Info about %1</source>
        <translation>מידע אודות %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="364"/>
        <source>Search</source>
        <translation>חיפוש</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="383"/>
        <source>Mute volume</source>
        <translation>השתקת השמע</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="412"/>
        <location filename="src/mainwindow.cpp" line="1365"/>
        <source>&amp;Downloads</source>
        <translation>הו&amp;רדות</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="413"/>
        <source>Show details about video downloads</source>
        <translation>הצגת פרטים על אודות הורדות וידאו</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="421"/>
        <source>&amp;Download</source>
        <translation>הו&amp;רדה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="422"/>
        <source>Download the current video</source>
        <translation>הורדת סרטון הווידאו הנוכחי</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="441"/>
        <source>Share the current video using %1</source>
        <translation>שיתוף הווידאו הנוכחי דרך %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="458"/>
        <source>&amp;Email</source>
        <translation>&amp;דוא״ל</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="459"/>
        <source>Email</source>
        <translation>דוא״ל</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="463"/>
        <source>&amp;Close</source>
        <translation>&amp;סגירה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="473"/>
        <source>&amp;Float on Top</source>
        <translation>&amp;ציפה מלמעלה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="478"/>
        <source>&amp;Stop After This Video</source>
        <translation>ל&amp;עצור לאחר וידאו זה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="485"/>
        <source>&amp;Report an Issue...</source>
        <translation>&amp;דיווח על תקלה...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="489"/>
        <source>&amp;Refine Search...</source>
        <translation>&amp;שיפור החיפוש...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="501"/>
        <source>More...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="504"/>
        <source>&amp;Related Videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="506"/>
        <source>Watch videos related to the current one</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="544"/>
        <source>&amp;Application</source>
        <translation>&amp;יישום</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="515"/>
        <source>Buy %1...</source>
        <translation>רכישת %1...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="558"/>
        <source>&amp;Playback</source>
        <translation>נ&amp;גינה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="572"/>
        <source>&amp;Playlist</source>
        <translation>&amp;רשימת השמעה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="581"/>
        <source>&amp;Video</source>
        <translation>&amp;וידאו</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="594"/>
        <source>&amp;View</source>
        <translation>&amp;צפייה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="601"/>
        <source>&amp;Share</source>
        <translation>&amp;שיתוף</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;עזרה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="690"/>
        <source>Press %1 to raise the volume, %2 to lower it</source>
        <translation>ניתן ללחוץ על %1 כדי להגביר את עצמת השמע, %2 כדי להנמיך אותה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="889"/>
        <location filename="src/mainwindow.cpp" line="895"/>
        <source>Opening %1</source>
        <translation>%1 נפתח</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="936"/>
        <source>Do you want to exit %1 with a download in progress?</source>
        <translation>האם ברצונך לצאת מ־%1 כאשר ישנה הורדה פעילה?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="937"/>
        <source>If you close %1 now, this download will be cancelled.</source>
        <translation>אם %1 ייסגר כעת, הורדה זו תבוטל.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="942"/>
        <source>Close and cancel download</source>
        <translation>סגירה וביטול ההורדה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="943"/>
        <source>Wait for download to finish</source>
        <translation>יש להמתין לסיום ההורדה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1102"/>
        <source>Leave &amp;Full Screen</source>
        <translation>יציאה ממ&amp;סך מלא</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1475"/>
        <source>%1 version %2 is now available.</source>
        <translation>%1 בגרסה %2 זמין כעת.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1479"/>
        <source>Remind me later</source>
        <translation>להזכיר לי מאוחר יותר</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1480"/>
        <source>Update</source>
        <translation>עדכון</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="982"/>
        <source>Error: %1</source>
        <translation>שגיאה: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="222"/>
        <source>P&amp;revious</source>
        <translation>ה&amp;קודם</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="223"/>
        <source>Go back to the previous track</source>
        <translation>חזרה לרצועה הקודמת</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="262"/>
        <source>&amp;Compact Mode</source>
        <translation>מ&amp;צב חסכוני</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="275"/>
        <source>Open the &amp;YouTube Page</source>
        <translation>פתיחת העמוד ב&amp;יוטיוב</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="282"/>
        <source>Copy the YouTube &amp;Link</source>
        <translation>העתקת ה&amp;קישור ליוטיוב</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="289"/>
        <source>Copy the Video Stream &amp;URL</source>
        <translation>העתקת כתובת ת&amp;זרים הווידאו</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="296"/>
        <source>Find Video &amp;Parts</source>
        <translation>חיפוש ק&amp;טעי וידאו</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>&amp;Clear Recent Searches</source>
        <translation>&amp;ניקוי החיפושים האחרונים</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="348"/>
        <source>Make a &amp;Donation</source>
        <translation>ה&amp;גשת תרומה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="405"/>
        <source>&amp;Manually Start Playing</source>
        <translation>התחלת נגינה י&amp;דנית</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="406"/>
        <source>Manually start playing videos</source>
        <translation>התחלה ידנית בנגינת קטעי הווידאו</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="728"/>
        <source>Choose your content location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1002"/>
        <source>&amp;Play</source>
        <translation>&amp;נגינה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1003"/>
        <source>Resume playback</source>
        <translation>המשך הנגינה</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1236"/>
        <source>Remaining time: %1</source>
        <translation>הזמן הנותר: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1294"/>
        <source>Volume at %1%</source>
        <translation>עצמת השמע היא %1%</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1300"/>
        <source>Volume is muted</source>
        <translation>השמע מושתק</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1303"/>
        <source>Volume is unmuted</source>
        <translation>השמע אינו מושתק</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1310"/>
        <source>Maximum video definition set to %1</source>
        <translation>איכות הנגינה המירבית מוגדרת ל־%1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1351"/>
        <source>Your privacy is now safe</source>
        <translation>פרטיותך מוגנת כעת.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1366"/>
        <source>Downloads complete</source>
        <translation>ההורדות הושלמו</translation>
    </message>
</context>
<context>
    <name>MediaView</name>
    <message>
        <location filename="src/mediaview.cpp" line="553"/>
        <source>You can now paste the YouTube link into another application</source>
        <translation>כעת ניתן להדביק את קישור ה־YouTube שלכם ליישום אחר</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="561"/>
        <source>You can now paste the video stream URL into another application</source>
        <translation>כעת ניתן להדביק את כתובת תזרים הווידאו ליישום אחר</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="562"/>
        <source>The link will be valid only for a limited time.</source>
        <translation>הקישור יהיה תקף לזמן מוגבל בלבד.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="636"/>
        <source>This is just the demo version of %1.</source>
        <translation>זוהי רק גרסת ההדגמה של %1.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="637"/>
        <source>It allows you to test the application and see if it works for you.</source>
        <translation>גרסה זו מאפשרת לך לבחון את היישום ולראות האם הוא מתאים לצרכיך.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="667"/>
        <source>Continue</source>
        <translation>המשך</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="756"/>
        <source>of</source>
        <comment>Used in video parts, as in '2 of 3'</comment>
        <translation>מתוך</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="768"/>
        <source>part</source>
        <comment>This is for video parts, as in 'Cool video - part 1'</comment>
        <translation>חלק</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="770"/>
        <source>episode</source>
        <comment>This is for video parts, as in 'Cool series - episode 1'</comment>
        <translation>פרק</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="847"/>
        <source>Sent from %1</source>
        <translation>נשלח דרך %1</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="644"/>
        <source>Get the full version</source>
        <translation>קבלת הגרסה המלאה</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="681"/>
        <source>Downloading %1</source>
        <translation>%1 מתקבל</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="21"/>
        <source>A new version of %1 is available!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="28"/>
        <source>%1 %2 is now available. You have %3.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="33"/>
        <source>Would you like to download it now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="39"/>
        <source>Skip This Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="43"/>
        <source>Remind Me Later</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="47"/>
        <source>Install Update</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PasteLineEdit</name>
    <message>
        <location filename="local/src/pastelineedit.cpp" line="6"/>
        <source>Paste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="159"/>
        <source>%1 views</source>
        <translation>%1 צפיות</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="285"/>
        <source>%1 of %2 (%3) — %4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="292"/>
        <source>Preparing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="294"/>
        <source>Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="296"/>
        <source>Completed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="298"/>
        <source>Stopped</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="334"/>
        <source>Stop downloading</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="344"/>
        <source>Show in %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="346"/>
        <source>Open parent folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="355"/>
        <source>Restart downloading</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="src/playlistmodel.cpp" line="52"/>
        <source>Searching...</source>
        <translation>בהליכי חיפוש...</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="53"/>
        <source>Show %1 More</source>
        <translation>הצגת %1 נוספים</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="54"/>
        <source>No videos</source>
        <translation>אין סרטונים</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="55"/>
        <source>No more videos</source>
        <translation>אין עוד סרטונים</translation>
    </message>
</context>
<context>
    <name>RefineSearchWidget</name>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="31"/>
        <source>Sort by</source>
        <translation>סידור לפי</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="35"/>
        <source>Relevance</source>
        <translation>רלוונטיות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="36"/>
        <location filename="src/refinesearchwidget.cpp" line="52"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="37"/>
        <source>View Count</source>
        <translation>מונה צפיות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="38"/>
        <source>Rating</source>
        <translation>דירוג</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="56"/>
        <source>Anytime</source>
        <translation>בכל זמן שהוא</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="57"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="58"/>
        <source>7 Days</source>
        <translation>7 ימים</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="59"/>
        <source>30 Days</source>
        <translation>30 ימים</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="73"/>
        <source>Duration</source>
        <translation>משך</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="77"/>
        <location filename="src/refinesearchwidget.cpp" line="104"/>
        <source>All</source>
        <translation>הכול</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="78"/>
        <source>Short</source>
        <translation>קצר</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="79"/>
        <source>Medium</source>
        <translation>בינוני</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="80"/>
        <source>Long</source>
        <translation>ארוך</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="83"/>
        <source>Less than 4 minutes</source>
        <translation>פחות מ־4 דקות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="84"/>
        <source>Between 4 and 20 minutes</source>
        <translation>בין 4 ל־20 דקות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="85"/>
        <source>Longer than 20 minutes</source>
        <translation>למעלה מ־20 דקות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="100"/>
        <source>Quality</source>
        <translation>איכות</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="105"/>
        <source>High Definition</source>
        <translation>אבחנה גבוהה (HD)</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="108"/>
        <source>720p or higher</source>
        <translation>720p ומעלה</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="122"/>
        <source>Done</source>
        <translation>הסתיים</translation>
    </message>
</context>
<context>
    <name>RegionsView</name>
    <message>
        <location filename="src/regionsview.cpp" line="19"/>
        <source>Done</source>
        <translation>הסתיים</translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="177"/>
        <source>Search</source>
        <translation>חיפוש</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="src/searchview.cpp" line="71"/>
        <source>Welcome to &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</source>
        <translation>ברוך בואך אל &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="50"/>
        <source>Get the full version</source>
        <translation>קבלת הגרסה המלאה</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="85"/>
        <source>Enter</source>
        <extracomment>&quot;Enter&quot;, as in &quot;type&quot;. The whole phrase says: &quot;Enter a keyword to start watching videos&quot;</extracomment>
        <translation>הזנה</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="90"/>
        <source>a keyword</source>
        <translation>מילת מפתח</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="91"/>
        <source>a channel</source>
        <translation>ערוץ</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="96"/>
        <source>to start watching videos.</source>
        <translation>כדי להתחיל לצפות בסרטונים.</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="120"/>
        <source>Watch</source>
        <translation>צפייה</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="138"/>
        <source>Recent keywords</source>
        <translation>מילות מפתח אחרונות</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="151"/>
        <source>Recent channels</source>
        <translation>ערוצים אחרונים</translation>
    </message>
</context>
<context>
    <name>SidebarHeader</name>
    <message>
        <location filename="src/sidebarheader.cpp" line="16"/>
        <location filename="src/sidebarheader.cpp" line="23"/>
        <source>&amp;Back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="57"/>
        <source>Forward to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="70"/>
        <source>Back to %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <location filename="src/sidebarwidget.cpp" line="52"/>
        <source>Refine Search</source>
        <translation>שיפור החיפוש</translation>
    </message>
    <message>
        <location filename="src/sidebarwidget.cpp" line="148"/>
        <source>Did you mean: %1</source>
        <translation>האם כוונתך הייתה: %1</translation>
    </message>
</context>
<context>
    <name>StandardFeedsView</name>
    <message>
        <location filename="src/standardfeedsview.cpp" line="85"/>
        <source>Most Popular</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="86"/>
        <source>Featured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="87"/>
        <source>Most Shared</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="88"/>
        <source>Most Discussed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="89"/>
        <source>Top Rated</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Video</name>
    <message>
        <location filename="src/video.cpp" line="213"/>
        <source>Cannot get video stream for %1</source>
        <translation>לא ניתן לקבל את תזרים הווידאו עבור %1</translation>
    </message>
    <message>
        <location filename="src/video.cpp" line="231"/>
        <source>Network error: %1 for %2</source>
        <translation>שגיאת רשת: %1 עבור %2</translation>
    </message>
</context>
<context>
    <name>YTRegions</name>
    <message>
        <location filename="src/ytregions.cpp" line="8"/>
        <source>Algeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="9"/>
        <source>Argentina</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="10"/>
        <source>Australia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="11"/>
        <source>Belgium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="12"/>
        <source>Brazil</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="13"/>
        <source>Canada</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="14"/>
        <source>Chile</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="15"/>
        <source>Colombia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="16"/>
        <source>Czech Republic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="17"/>
        <source>Egypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="18"/>
        <source>France</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="19"/>
        <source>Germany</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="20"/>
        <source>Ghana</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="21"/>
        <source>Greece</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="22"/>
        <source>Hong Kong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="23"/>
        <source>Hungary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="24"/>
        <source>India</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="25"/>
        <source>Indonesia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="26"/>
        <source>Ireland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="27"/>
        <source>Israel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="28"/>
        <source>Italy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="29"/>
        <source>Japan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="30"/>
        <source>Jordan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="31"/>
        <source>Kenya</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="32"/>
        <source>Malaysia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="33"/>
        <source>Mexico</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="34"/>
        <source>Morocco</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="35"/>
        <source>Netherlands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="36"/>
        <source>New Zealand</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="37"/>
        <source>Nigeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="38"/>
        <source>Peru</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="39"/>
        <source>Philippines</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="40"/>
        <source>Poland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="41"/>
        <source>Russia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="42"/>
        <source>Saudi Arabia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="43"/>
        <source>Singapore</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="44"/>
        <source>South Africa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="45"/>
        <source>South Korea</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="46"/>
        <source>Spain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="47"/>
        <source>Sweden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="48"/>
        <source>Taiwan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="49"/>
        <source>Tunisia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="50"/>
        <source>Turkey</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="51"/>
        <source>Uganda</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="52"/>
        <source>United Arab Emirates</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="53"/>
        <source>United Kingdom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="54"/>
        <source>Yemen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="128"/>
        <source>Worldwide</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>