<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutView</name>
    <message>
        <location filename="src/aboutview.cpp" line="32"/>
        <source>There&apos;s life outside the browser!</source>
        <translation>Υπάρχει ζωή έξω απο τον πλοηγό!</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="33"/>
        <source>Version %1</source>
        <translation>Έκδοση %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="38"/>
        <source>Licensed to: %1</source>
        <translation>Αδειοδοτημένο στον/ην: %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="42"/>
        <source>%1 is Free Software but its development takes precious time.</source>
        <translation>Το %1 είναι Ελεύθερο Λογισμικό αλλά η ανάπτυξη του παίρνει πολύτιμο χρόνο.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="43"/>
        <source>Please &lt;a href=&apos;%1&apos;&gt;donate&lt;/a&gt; to support the continued development of %2.</source>
        <translation>Παρακαλούμε &lt;a href=&apos;%1%&apos;&gt;δωρίστε&lt;/a&gt; για να υποστηρίξετε την συνεχόμενη ανάπτυξη του %2.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="47"/>
        <source>You may want to try my other apps as well:</source>
        <translation>Μπορεί να θέλετε να δοκιμάσετε και τις άλλες μου εφαρμογές:</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="50"/>
        <source>%1, a YouTube music player</source>
        <translation>%1, ένας αναπαραγωγέας μουσικής του YouTube</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="54"/>
        <source>%1, a music player</source>
        <translation>%1, ένας αναπαραγωγέας μουσικής</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="60"/>
        <source>Translate %1 to your native language using %2</source>
        <translation>Μεταφράστε το %1 στη γλώσσα σας χρησιμοποιώντας το %2</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="65"/>
        <source>Icon designed by %1.</source>
        <translation>Σχεδιασμός εικονιδίου από %1.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="69"/>
        <source>Released under the &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</source>
        <translation>Έκδοση κάτω απο τους όρους της &lt;a href=&apos;%1&apos;&gt;Γενικής Άδειας Χρήσης GNU&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="84"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="17"/>
        <source>About</source>
        <translation>Σχετικά</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="19"/>
        <source>What you always wanted to know about %1 and never dared to ask</source>
        <translation>Ότι θέλατε να μάθετε σχετικά με το %1 και δεν τολμούσατε να ρωτήσετε</translation>
    </message>
</context>
<context>
    <name>ActivationDialog</name>
    <message>
        <location filename="local/src/activationdialog.cpp" line="17"/>
        <source>Enter your License Details</source>
        <translation>Εισάγετε τις λεπτομέρειες της άδειας χρήσης</translation>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="29"/>
        <source>&amp;Email:</source>
        <translation>&amp;Email:</translation>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="35"/>
        <source>&amp;Code:</source>
        <translation>&amp;Κωδικός:</translation>
    </message>
</context>
<context>
    <name>ActivationView</name>
    <message>
        <location filename="local/src/activationview.cpp" line="47"/>
        <source>Please license %1</source>
        <translation>Παρακαλώ αποκτήστε την άδεια χρήσης του %1</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="51"/>
        <source>This demo has expired.</source>
        <translation>Αυτή η δοκιμαστική έκδοση έληξε.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="53"/>
        <source>The full version allows you to download videos longer than %1 minutes and to watch videos without interruptions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="55"/>
        <source>Without a license, the application will expire in %1 days.</source>
        <translation>Χωρίς την άδεια χρήσης, η εφαρμογή θα λήξη σε %1 μέρες</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="57"/>
        <source>By purchasing the full version, you will also support the hard work I put into creating %1.</source>
        <translation>Αγοράζοντας την πλήρη έκδοση, υποστηρίζετε επίσης την σκληρή δουλειά που έχω κάνει για τη δημιουργία του %1.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="70"/>
        <source>Use Demo</source>
        <translation>Χρήση της δοκιμαστικής έκδοσης</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="77"/>
        <source>Enter License</source>
        <translation>Εισαγωγή άδειας χρήσης</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="85"/>
        <source>Buy License</source>
        <translation>Αγορά άδειας χρήσης</translation>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="56"/>
        <source>Clear</source>
        <translation>Καθαρισμός</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="src/downloaditem.cpp" line="322"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="325"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="328"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="337"/>
        <source>bytes/sec</source>
        <translation>bytes/δευτ</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="340"/>
        <source>KB/sec</source>
        <translation>KB/δευτ</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="343"/>
        <source>MB/sec</source>
        <translation>MB/δευτ</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="349"/>
        <source>seconds</source>
        <translation>δευτερόλεπτα</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="352"/>
        <source>minutes</source>
        <translation>λεπτά</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="355"/>
        <source>%4 %5 remaining</source>
        <translation>απομένουν %4 %5</translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="55"/>
        <source>This is just the demo version of %1.</source>
        <translation>Αυτή είναι απλά η δοκιμαστική έκδοση του %1.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="57"/>
        <source>It can only download videos shorter than %1 minutes so you can test the download functionality.</source>
        <translation>Μπορεί να κατεβάσει βίντεο μικρότερα από %1 λεπτά ώστε να δοκιμάσετε τη λειτουργία κατεβάσματος.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="63"/>
        <source>Continue</source>
        <translation>Συνέχεια</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="64"/>
        <source>Get the full version</source>
        <translation>Αποκτήστε την πλήρη έκδοση</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="150"/>
        <source>%1 downloaded in %2</source>
        <translation>%1 λήφθηκε σε %2</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="153"/>
        <source>Download finished</source>
        <translation>Η λήψη ολοκληρώθηκε</translation>
    </message>
    <message numerus="yes">
        <location filename="src/downloadmanager.cpp" line="159"/>
        <source>%n Download(s)</source>
        <translation><numerusform>%n Λήψη</numerusform><numerusform>%n Λήψεις</numerusform></translation>
    </message>
</context>
<context>
    <name>DownloadSettings</name>
    <message>
        <location filename="src/downloadsettings.cpp" line="15"/>
        <source>Change location...</source>
        <translation>Αλλάξτε τοποθεσία...</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="47"/>
        <source>Choose the download location</source>
        <translation>Επιλέξτε την τοποθεσία λήψης</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="59"/>
        <source>Download location changed.</source>
        <translation>Η τοποθεσία λήψης άλλαξε.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="61"/>
        <source>Current downloads will still go in the previous location.</source>
        <translation>Τα ήδη ληφθέντα θα παραμείνουν στην προηγούμενη τοποθεσία.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="72"/>
        <source>Downloading to: %1</source>
        <translation>Λήψη στο: %1</translation>
    </message>
</context>
<context>
    <name>DownloadView</name>
    <message>
        <location filename="src/downloadview.cpp" line="18"/>
        <location filename="src/downloadview.h" line="22"/>
        <source>Downloads</source>
        <translation>Λήψεις</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="60"/>
        <source>Downloading update...</source>
        <translation>Μεταφόρτωση ενημερώσεων...</translation>
    </message>
</context>
<context>
    <name>GlobalShortcuts</name>
    <message>
        <location filename="src/globalshortcuts.cpp" line="16"/>
        <source>Play</source>
        <translation>Αναπαραγωγή</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="17"/>
        <source>Pause</source>
        <translation>Παύση</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="18"/>
        <source>Play/Pause</source>
        <translation>Αναπαραγωγή/Παύση</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="19"/>
        <source>Stop</source>
        <translation>Διακοπή</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="20"/>
        <source>Stop playing after current track</source>
        <translation>Διακοπή αναπαραγωγής μετά το τρέχον κομμάτι</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="21"/>
        <source>Next track</source>
        <translation>Επόμενο κομμάτι</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="22"/>
        <source>Previous track</source>
        <translation>Προηγούμενο κομμάτι</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="23"/>
        <source>Increase volume</source>
        <translation>Αύξηση έντασης</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="24"/>
        <source>Decrease volume</source>
        <translation>Μείωση έντασης</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="25"/>
        <source>Mute</source>
        <translation>Σίγαση</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="26"/>
        <source>Seek forward</source>
        <translation>Αναζήτηση μπροστά</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="27"/>
        <source>Seek backward</source>
        <translation>Αναζήτηση πίσω</translation>
    </message>
</context>
<context>
    <name>HomeView</name>
    <message>
        <location filename="src/homeview.cpp" line="33"/>
        <source>Search</source>
        <translation>Αναζήτηση</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="35"/>
        <source>Find videos and channels by keyword</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="40"/>
        <source>Browse</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="42"/>
        <source>Browse videos by category</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.h" line="22"/>
        <source>Make yourself comfortable</source>
        <translation>Βολευτείτε</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="src/loadingwidget.cpp" line="90"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.cpp" line="213"/>
        <source>&amp;Stop</source>
        <translation>&amp;Διακοπή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="214"/>
        <source>Stop playback and go back to the search view</source>
        <translation>Διακοπή αναπαραγωγής και επιστροφή στην προβολή αναζήτησης</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="232"/>
        <source>S&amp;kip</source>
        <translation>&amp;Παράλειψη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="233"/>
        <source>Skip to the next video</source>
        <translation>Μετάβαση στο επόμενο βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="239"/>
        <location filename="src/mainwindow.cpp" line="989"/>
        <source>&amp;Pause</source>
        <translation>&amp;Παύση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="240"/>
        <location filename="src/mainwindow.cpp" line="990"/>
        <source>Pause playback</source>
        <translation>Παύση αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="246"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Πλήρης οθόνη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Go full screen</source>
        <translation>Προβολή σε πλήρη οθόνη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="263"/>
        <source>Hide the playlist and the toolbar</source>
        <translation>Απόκρυψη της λίστας αναπαραγωγής και της εργαλειοθήκης</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="276"/>
        <source>Go to the YouTube video page and pause playback</source>
        <translation>Μετάβαση στη σελίδα βίντεο του YouTube και παύση αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="283"/>
        <source>Copy the current video YouTube link to the clipboard</source>
        <translation>Αντιγραφή του δεσμού του τρέχοντος YouTube βίντεο στη μνήμη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="290"/>
        <source>Copy the current video stream URL to the clipboard</source>
        <translation>Αντιγραφή του URL της τρέχουσας ροής βίντεο στη μνήμη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="297"/>
        <source>Find other video parts hopefully in the right order</source>
        <translation>Εύρεση των υπόλοιπων επισοδείων του βίντεο, ελπίζουμε στη σωστή σειρά</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="303"/>
        <source>&amp;Remove</source>
        <translation>&amp;Αφαίρεση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="304"/>
        <source>Remove the selected videos from the playlist</source>
        <translation>Αφαίρεση επιλεγμένων βίντεο απο την λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="310"/>
        <source>Move &amp;Up</source>
        <translation>Μετακίνηση προς τα &amp;πάνω</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="311"/>
        <source>Move up the selected videos in the playlist</source>
        <translation>Μετακίνηση επιλεγμένων βίντεο προς τα πάνω</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="317"/>
        <source>Move &amp;Down</source>
        <translation>Μετακίνηση προς τα &amp;κάτω</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="318"/>
        <source>Move down the selected videos in the playlist</source>
        <translation>Μετακίνηση επιλεγμένων βίντεο προς τα κάτω</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="329"/>
        <source>Clear the search history. Cannot be undone.</source>
        <translation>Καθαρισμός του ιστορικού αναζήτησης. Δεν μπορεί να γίνει επαναφορά.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>Έ&amp;ξοδος</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="337"/>
        <source>Bye</source>
        <translation>Γεια</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="341"/>
        <source>&amp;Website</source>
        <translation>&amp;Ιστοχώρος</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="343"/>
        <source>%1 on the Web</source>
        <translation>Το %1 στο διαδίκτυο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="349"/>
        <source>Please support the continued development of %1</source>
        <translation>Παρακαλούμε υποστηρίξτε την συνεχόμενη ανάπτυξη του %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="354"/>
        <source>&amp;About</source>
        <translation>&amp;Σχετικά</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="356"/>
        <source>Info about %1</source>
        <translation>Πληροφορίες για %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="364"/>
        <source>Search</source>
        <translation>Αναζήτηση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="383"/>
        <source>Mute volume</source>
        <translation>Σίγαση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="412"/>
        <location filename="src/mainwindow.cpp" line="1365"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Λήψεις</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="413"/>
        <source>Show details about video downloads</source>
        <translation>Εμφάνιση λεπτομερειών για τις λήψεις βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="421"/>
        <source>&amp;Download</source>
        <translation>&amp;Λήψη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="422"/>
        <source>Download the current video</source>
        <translation>Λήψη του τρέχοντος βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="441"/>
        <source>Share the current video using %1</source>
        <translation>Κοινή χρήση του τρέχοντος βίντεο με το %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="458"/>
        <source>&amp;Email</source>
        <translation>&amp;Ηλ. αλληλογραφία</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="459"/>
        <source>Email</source>
        <translation>Ηλ. αλληλογραφία</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="463"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="473"/>
        <source>&amp;Float on Top</source>
        <translation>&amp;Διατήρηση στην κορυφή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="478"/>
        <source>&amp;Stop After This Video</source>
        <translation>&amp;Διακοπή μετά από αυτό το βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="485"/>
        <source>&amp;Report an Issue...</source>
        <translation>&amp;Αναφέρετε κάποιο σφάλμα...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="489"/>
        <source>&amp;Refine Search...</source>
        <translation>&amp;Σύνθετη αναζήτηση...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="501"/>
        <source>More...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="504"/>
        <source>&amp;Related Videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="506"/>
        <source>Watch videos related to the current one</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="544"/>
        <source>&amp;Application</source>
        <translation>&amp;Εφαρμογή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="515"/>
        <source>Buy %1...</source>
        <translation>Αγοράστε το %1...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="558"/>
        <source>&amp;Playback</source>
        <translation>&amp;Αναπαραγωγή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="572"/>
        <source>&amp;Playlist</source>
        <translation>Λ&amp;ίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="581"/>
        <source>&amp;Video</source>
        <translation>Βίν&amp;τεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="594"/>
        <source>&amp;View</source>
        <translation>&amp;Προβολή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="601"/>
        <source>&amp;Share</source>
        <translation>&amp;Κοινή χρήση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Βοήθεια</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="690"/>
        <source>Press %1 to raise the volume, %2 to lower it</source>
        <translation>Πατήστε %1 για να αυξήσετε την ένταση, %2 για να την χαμηλώσετε</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="889"/>
        <location filename="src/mainwindow.cpp" line="895"/>
        <source>Opening %1</source>
        <translation>Άνοιγμα %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="936"/>
        <source>Do you want to exit %1 with a download in progress?</source>
        <translation>Θέλετε να κλείσετε το %1 ενώ βρίσκεται μια λήψη σε εξέλιξη;</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="937"/>
        <source>If you close %1 now, this download will be cancelled.</source>
        <translation>Αν κλείσετε το %1 τώρα, η λήψη θα ακυρωθεί.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="942"/>
        <source>Close and cancel download</source>
        <translation>Κλείσιμο και ακύρωση λήψης</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="943"/>
        <source>Wait for download to finish</source>
        <translation>Αναμονή ολοκλήρωσης λήψης</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1102"/>
        <source>Leave &amp;Full Screen</source>
        <translation>Έξοδος από &amp;πλήρη οθόνη</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1475"/>
        <source>%1 version %2 is now available.</source>
        <translation>Η έκδοση %2 του %1 είναι διαθέσιμη.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1479"/>
        <source>Remind me later</source>
        <translation>Υπενθύμιση αργότερα</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1480"/>
        <source>Update</source>
        <translation>Ενημέρωση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="982"/>
        <source>Error: %1</source>
        <translation>Σφάλμα: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="222"/>
        <source>P&amp;revious</source>
        <translation>Π&amp;ροηγούμενο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="223"/>
        <source>Go back to the previous track</source>
        <translation>Επιστροφή στο προηγούμενο κομμάτι</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="262"/>
        <source>&amp;Compact Mode</source>
        <translation>&amp;Συμπαγής εμφάνιση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="275"/>
        <source>Open the &amp;YouTube Page</source>
        <translation>Άνοιγμα στην ιστοσελίδα του &amp;YouTube</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="282"/>
        <source>Copy the YouTube &amp;Link</source>
        <translation>Αντιγραφή του &amp;δεσμού YouTube</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="289"/>
        <source>Copy the Video Stream &amp;URL</source>
        <translation>Αντιγραφή του &amp;URL της ροής βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="296"/>
        <source>Find Video &amp;Parts</source>
        <translation>Αναζήτηση των &amp;επισοδείων του βίντεο</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>&amp;Clear Recent Searches</source>
        <translation>&amp;Καθαρισμός πρόσφατων αναζητήσεων</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="348"/>
        <source>Make a &amp;Donation</source>
        <translation>Κάντε μια &amp;δωρεά</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="405"/>
        <source>&amp;Manually Start Playing</source>
        <translation>Εκκίνηση της αναπαραγωγής &amp;χειροκίνητα</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="406"/>
        <source>Manually start playing videos</source>
        <translation>Εκκίνηση αναπαραγωγής των βίντεο χειροκίνητα</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="728"/>
        <source>Choose your content location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1002"/>
        <source>&amp;Play</source>
        <translation>Α&amp;ναπαραγωγή</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1003"/>
        <source>Resume playback</source>
        <translation>Συνέχεια αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1236"/>
        <source>Remaining time: %1</source>
        <translation>Υπολειπόμενος χρόνος: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1294"/>
        <source>Volume at %1%</source>
        <translation>Ένταση στο %1%</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1300"/>
        <source>Volume is muted</source>
        <translation>Σίγαση</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1303"/>
        <source>Volume is unmuted</source>
        <translation>Η ένταση αποκαταστάθηκε</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1310"/>
        <source>Maximum video definition set to %1</source>
        <translation>H μέγιστη ανάλυση βίντεο τέθηκε σε %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1351"/>
        <source>Your privacy is now safe</source>
        <translation>Η ιδιωτικότητα σας είναι τώρα ασφαλής</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1366"/>
        <source>Downloads complete</source>
        <translation>Οι λήψεις ολοκληρώθηκαν</translation>
    </message>
</context>
<context>
    <name>MediaView</name>
    <message>
        <location filename="src/mediaview.cpp" line="553"/>
        <source>You can now paste the YouTube link into another application</source>
        <translation>Τώρα μπορείτε να επικολλήσετε το δεσμό του YouTube σε κάποια άλλη εφαρμογή</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="561"/>
        <source>You can now paste the video stream URL into another application</source>
        <translation>Τώρα μπορείτε να επικολλήσετε το URL της ροής βίντεο σε κάποια άλλη εφαρμογή</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="562"/>
        <source>The link will be valid only for a limited time.</source>
        <translation>Ο σύνδεμος θα είναι έγκυρος μόνο για περιορισμένο χρονικό διάστημα.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="636"/>
        <source>This is just the demo version of %1.</source>
        <translation>Αυτή είναι απλά μια δοκιμαστική έκδοση του %1.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="637"/>
        <source>It allows you to test the application and see if it works for you.</source>
        <translation>Σαε επιτρέπει να δοκιμάσετε την εφαρμογή και να δείτε αν σας κάνει.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="667"/>
        <source>Continue</source>
        <translation>Συνέχεια</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="756"/>
        <source>of</source>
        <comment>Used in video parts, as in '2 of 3'</comment>
        <translation>από</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="768"/>
        <source>part</source>
        <comment>This is for video parts, as in 'Cool video - part 1'</comment>
        <translation>μέρος</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="770"/>
        <source>episode</source>
        <comment>This is for video parts, as in 'Cool series - episode 1'</comment>
        <translation>επισόδειο</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="847"/>
        <source>Sent from %1</source>
        <translation>Αποστολή από %1</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="644"/>
        <source>Get the full version</source>
        <translation>Αποκτήστε την πλήρη έκδοση</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="681"/>
        <source>Downloading %1</source>
        <translation>Λήψη %1</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="21"/>
        <source>A new version of %1 is available!</source>
        <translation>Μια νέα έκδοση του %1 είναι διαθέσιμη!</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="28"/>
        <source>%1 %2 is now available. You have %3.</source>
        <translation>Η %1 %2 είναι διαθέσιμη. Έχετε την %3</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="33"/>
        <source>Would you like to download it now?</source>
        <translation>Θα θέλατε να την κατεβάσετε τώρα;</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="39"/>
        <source>Skip This Version</source>
        <translation>Παράλειψη αυτής της έκδοσης</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="43"/>
        <source>Remind Me Later</source>
        <translation>Υπενθύμισε το μου αργότερα</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="47"/>
        <source>Install Update</source>
        <translation>Εγκατάσταση Ενημερώσεων</translation>
    </message>
</context>
<context>
    <name>PasteLineEdit</name>
    <message>
        <location filename="local/src/pastelineedit.cpp" line="6"/>
        <source>Paste</source>
        <translation>Επικόλληση</translation>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="159"/>
        <source>%1 views</source>
        <translation>Προβολές %1</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="285"/>
        <source>%1 of %2 (%3) — %4</source>
        <translation>%1 από %2 (%3) — %4</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="292"/>
        <source>Preparing</source>
        <translation>Προετοιμάζεται</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="294"/>
        <source>Failed</source>
        <translation>Απέτυχε</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="296"/>
        <source>Completed</source>
        <translation>Ολοκληρώθηκε</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="298"/>
        <source>Stopped</source>
        <translation>Διακόπηκε</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="334"/>
        <source>Stop downloading</source>
        <translation>Διακοπή λήψης</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="344"/>
        <source>Show in %1</source>
        <translation>Εμφάνιση σε %1</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="346"/>
        <source>Open parent folder</source>
        <translation>Άνοιγμα γονικού φακέλου</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="355"/>
        <source>Restart downloading</source>
        <translation>Επανεκκίνηση λήψης</translation>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="src/playlistmodel.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Αναζήτηση...</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="53"/>
        <source>Show %1 More</source>
        <translation>Εμφάνιση %1 ακόμα</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="54"/>
        <source>No videos</source>
        <translation>Κανένα βίντεο</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="55"/>
        <source>No more videos</source>
        <translation>Δεν υπάρχουν άλλα βίντεο</translation>
    </message>
</context>
<context>
    <name>RefineSearchWidget</name>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="31"/>
        <source>Sort by</source>
        <translation>Ταξινόμηση κατά ...</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="35"/>
        <source>Relevance</source>
        <translation>Σχετικότητα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="36"/>
        <location filename="src/refinesearchwidget.cpp" line="52"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="37"/>
        <source>View Count</source>
        <translation>Αριθμό προβολών</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="38"/>
        <source>Rating</source>
        <translation>Βαθμολογία</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="56"/>
        <source>Anytime</source>
        <translation>Οποτεδήποτε</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="57"/>
        <source>Today</source>
        <translation>Σήμερα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="58"/>
        <source>7 Days</source>
        <translation>Εβδομάδα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="59"/>
        <source>30 Days</source>
        <translation>Μήνας</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="73"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="77"/>
        <location filename="src/refinesearchwidget.cpp" line="104"/>
        <source>All</source>
        <translation>Όλα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="78"/>
        <source>Short</source>
        <translation>Μικρό</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="79"/>
        <source>Medium</source>
        <translation>Μέτριο</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="80"/>
        <source>Long</source>
        <translation>Μεγάλο</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="83"/>
        <source>Less than 4 minutes</source>
        <translation>Λιγότερο από 4 λεπτά</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="84"/>
        <source>Between 4 and 20 minutes</source>
        <translation>Μεταξύ 4 και 20 λεπτών</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="85"/>
        <source>Longer than 20 minutes</source>
        <translation>Μεγαλύτερο από 20 λεπτά</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="100"/>
        <source>Quality</source>
        <translation>Ποιότητα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="105"/>
        <source>High Definition</source>
        <translation>Υψηλή ανάλυση</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="108"/>
        <source>720p or higher</source>
        <translation>720p είτε ψηλότερα</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="122"/>
        <source>Done</source>
        <translation>Έγινε</translation>
    </message>
</context>
<context>
    <name>RegionsView</name>
    <message>
        <location filename="src/regionsview.cpp" line="19"/>
        <source>Done</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="177"/>
        <source>Search</source>
        <translation>Αναζήτηση</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="src/searchview.cpp" line="71"/>
        <source>Welcome to &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</source>
        <translation>Καλωσορίσατε στο &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="50"/>
        <source>Get the full version</source>
        <translation>Αποκτήστε την πλήρη έκδοση</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="85"/>
        <source>Enter</source>
        <extracomment>&quot;Enter&quot;, as in &quot;type&quot;. The whole phrase says: &quot;Enter a keyword to start watching videos&quot;</extracomment>
        <translation>Πληκτρολογήστε</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="90"/>
        <source>a keyword</source>
        <translation>μια λέξη-κλειδί</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="91"/>
        <source>a channel</source>
        <translation>ένα κανάλι</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="96"/>
        <source>to start watching videos.</source>
        <translation>για να αρχίσετε να βλέπετε βίντεο.</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="120"/>
        <source>Watch</source>
        <translation>Δείτε</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="138"/>
        <source>Recent keywords</source>
        <translation>Πρόσφατες λέξεις-κλειδιά</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="151"/>
        <source>Recent channels</source>
        <translation>Πρόσφατα κανάλια</translation>
    </message>
</context>
<context>
    <name>SidebarHeader</name>
    <message>
        <location filename="src/sidebarheader.cpp" line="16"/>
        <location filename="src/sidebarheader.cpp" line="23"/>
        <source>&amp;Back</source>
        <translation>&amp;Πίσω</translation>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="57"/>
        <source>Forward to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="70"/>
        <source>Back to %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <location filename="src/sidebarwidget.cpp" line="52"/>
        <source>Refine Search</source>
        <translation>Σύνθετη αναζήτηση</translation>
    </message>
    <message>
        <location filename="src/sidebarwidget.cpp" line="148"/>
        <source>Did you mean: %1</source>
        <translation>Μήπως εννοείτε: %1</translation>
    </message>
</context>
<context>
    <name>StandardFeedsView</name>
    <message>
        <location filename="src/standardfeedsview.cpp" line="85"/>
        <source>Most Popular</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="86"/>
        <source>Featured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="87"/>
        <source>Most Shared</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="88"/>
        <source>Most Discussed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="89"/>
        <source>Top Rated</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Video</name>
    <message>
        <location filename="src/video.cpp" line="213"/>
        <source>Cannot get video stream for %1</source>
        <translation>Αδυναμία λήψης της ροής βίντεο για %1</translation>
    </message>
    <message>
        <location filename="src/video.cpp" line="231"/>
        <source>Network error: %1 for %2</source>
        <translation>Σφάλμα δικτύου: %1 για %2</translation>
    </message>
</context>
<context>
    <name>YTRegions</name>
    <message>
        <location filename="src/ytregions.cpp" line="8"/>
        <source>Algeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="9"/>
        <source>Argentina</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="10"/>
        <source>Australia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="11"/>
        <source>Belgium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="12"/>
        <source>Brazil</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="13"/>
        <source>Canada</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="14"/>
        <source>Chile</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="15"/>
        <source>Colombia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="16"/>
        <source>Czech Republic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="17"/>
        <source>Egypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="18"/>
        <source>France</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="19"/>
        <source>Germany</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="20"/>
        <source>Ghana</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="21"/>
        <source>Greece</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="22"/>
        <source>Hong Kong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="23"/>
        <source>Hungary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="24"/>
        <source>India</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="25"/>
        <source>Indonesia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="26"/>
        <source>Ireland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="27"/>
        <source>Israel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="28"/>
        <source>Italy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="29"/>
        <source>Japan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="30"/>
        <source>Jordan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="31"/>
        <source>Kenya</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="32"/>
        <source>Malaysia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="33"/>
        <source>Mexico</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="34"/>
        <source>Morocco</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="35"/>
        <source>Netherlands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="36"/>
        <source>New Zealand</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="37"/>
        <source>Nigeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="38"/>
        <source>Peru</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="39"/>
        <source>Philippines</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="40"/>
        <source>Poland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="41"/>
        <source>Russia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="42"/>
        <source>Saudi Arabia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="43"/>
        <source>Singapore</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="44"/>
        <source>South Africa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="45"/>
        <source>South Korea</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="46"/>
        <source>Spain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="47"/>
        <source>Sweden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="48"/>
        <source>Taiwan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="49"/>
        <source>Tunisia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="50"/>
        <source>Turkey</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="51"/>
        <source>Uganda</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="52"/>
        <source>United Arab Emirates</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="53"/>
        <source>United Kingdom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="54"/>
        <source>Yemen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="128"/>
        <source>Worldwide</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>