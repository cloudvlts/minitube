<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutView</name>
    <message>
        <location filename="src/aboutview.cpp" line="32"/>
        <source>There&apos;s life outside the browser!</source>
        <translation>Selaimen ulkopuolellakin on elämää!</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="33"/>
        <source>Version %1</source>
        <translation>Versio: %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="38"/>
        <source>Licensed to: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="42"/>
        <source>%1 is Free Software but its development takes precious time.</source>
        <translation>%1 on ilmainen ohjelma, mutta sen kehittäminen vie kallista aikaa.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="43"/>
        <source>Please &lt;a href=&apos;%1&apos;&gt;donate&lt;/a&gt; to support the continued development of %2.</source>
        <translation>Voit tukea %2-kehitystä jatkumista tekemällä &lt;a href=&apos;%1&apos;&gt;lahjoituksen&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="47"/>
        <source>You may want to try my other apps as well:</source>
        <translation>Sinua voi kiinnostaa myös nämä sovellukseni:</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="50"/>
        <source>%1, a YouTube music player</source>
        <translation>%1, YouTube-musiikkisoitin</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="54"/>
        <source>%1, a music player</source>
        <translation>%1, musiikkisoitin</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="60"/>
        <source>Translate %1 to your native language using %2</source>
        <translation>Käännä %1 äidinkielellesi käyttämällä %2</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="65"/>
        <source>Icon designed by %1.</source>
        <translation>Kuvakkeen suunnitteli %1.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="69"/>
        <source>Released under the &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</source>
        <translation>Julkaistu &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt; -lisenssillä</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="84"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="17"/>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="19"/>
        <source>What you always wanted to know about %1 and never dared to ask</source>
        <translation>Mitä olet aina halunnut tietää %1sta, muttet ole koskaan kehdannut kysyä</translation>
    </message>
</context>
<context>
    <name>ActivationDialog</name>
    <message>
        <location filename="local/src/activationdialog.cpp" line="17"/>
        <source>Enter your License Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="29"/>
        <source>&amp;Email:</source>
        <translation>&amp;Sähköposti:</translation>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="35"/>
        <source>&amp;Code:</source>
        <translation>&amp;Koodi:</translation>
    </message>
</context>
<context>
    <name>ActivationView</name>
    <message>
        <location filename="local/src/activationview.cpp" line="47"/>
        <source>Please license %1</source>
        <translation>Lisensoi %1</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="51"/>
        <source>This demo has expired.</source>
        <translation>Tämä koekäyttöversio on vanhentunut.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="53"/>
        <source>The full version allows you to download videos longer than %1 minutes and to watch videos without interruptions.</source>
        <translation>Täysversio sallii sinun ladata yli %1 minuutin pituisia videoita ja katsoa videoita ilman keskeytyksiä.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="55"/>
        <source>Without a license, the application will expire in %1 days.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="57"/>
        <source>By purchasing the full version, you will also support the hard work I put into creating %1.</source>
        <translation>Ostamalla täyden version tuet kovaa työtäni sovelluksen %1 parissa.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="70"/>
        <source>Use Demo</source>
        <translation>Käytä demoa</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="77"/>
        <source>Enter License</source>
        <translation>Syötä lisenssi</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="85"/>
        <source>Buy License</source>
        <translation>Osta lisenssi</translation>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="56"/>
        <source>Clear</source>
        <translation>Tyhjennä</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="src/downloaditem.cpp" line="322"/>
        <source>bytes</source>
        <translation>tavua</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="325"/>
        <source>KB</source>
        <translation>Kt</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="328"/>
        <source>MB</source>
        <translation>Mt</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="337"/>
        <source>bytes/sec</source>
        <translation>tavua/sekunnissa</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="340"/>
        <source>KB/sec</source>
        <translation>Kt/s</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="343"/>
        <source>MB/sec</source>
        <translation>Mt/s</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="349"/>
        <source>seconds</source>
        <translation>sekunttia</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="352"/>
        <source>minutes</source>
        <translation>minuuttia</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="355"/>
        <source>%4 %5 remaining</source>
        <translation>%4 %5 jäljellä</translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="55"/>
        <source>This is just the demo version of %1.</source>
        <translation>Tämä on vain %1-kokeiluversio.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="57"/>
        <source>It can only download videos shorter than %1 minutes so you can test the download functionality.</source>
        <translation>Voit ladata vain videoita jotka ovat lyhyempiä kuin %1 minuuttia, jotta voit testata latausominaisuutta.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="63"/>
        <source>Continue</source>
        <translation>Jatka</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="64"/>
        <source>Get the full version</source>
        <translation>Hanki täysi versio</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="150"/>
        <source>%1 downloaded in %2</source>
        <translation>%1 ladattu ajassa %2</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="153"/>
        <source>Download finished</source>
        <translation>Lataus valmistui</translation>
    </message>
    <message numerus="yes">
        <location filename="src/downloadmanager.cpp" line="159"/>
        <source>%n Download(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>DownloadSettings</name>
    <message>
        <location filename="src/downloadsettings.cpp" line="15"/>
        <source>Change location...</source>
        <translation>Vaihda sijaintia...</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="47"/>
        <source>Choose the download location</source>
        <translation>Valitse latausten sijainti</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="59"/>
        <source>Download location changed.</source>
        <translation>Lataus sijaintia on muutettu.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="61"/>
        <source>Current downloads will still go in the previous location.</source>
        <translation>Nykyiset lataukset menevät vanhaan sijaintiin.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="72"/>
        <source>Downloading to: %1</source>
        <translation>Ladataan kansioon: %1</translation>
    </message>
</context>
<context>
    <name>DownloadView</name>
    <message>
        <location filename="src/downloadview.cpp" line="18"/>
        <location filename="src/downloadview.h" line="22"/>
        <source>Downloads</source>
        <translation>Lataukset</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="60"/>
        <source>Downloading update...</source>
        <translation>Ladataan päivitystä...</translation>
    </message>
</context>
<context>
    <name>GlobalShortcuts</name>
    <message>
        <location filename="src/globalshortcuts.cpp" line="16"/>
        <source>Play</source>
        <translation>Toista</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="17"/>
        <source>Pause</source>
        <translation>Keskeytä</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="18"/>
        <source>Play/Pause</source>
        <translation>Toista/keskeytä</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="19"/>
        <source>Stop</source>
        <translation>Pysäytä</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="20"/>
        <source>Stop playing after current track</source>
        <translation>Pysäytä toisto nykyisen kappaleen jälkeen</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="21"/>
        <source>Next track</source>
        <translation>Seuraava kappale</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="22"/>
        <source>Previous track</source>
        <translation>Edellinen kappale</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="23"/>
        <source>Increase volume</source>
        <translation>Lisää äänenvoimakkuutta</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="24"/>
        <source>Decrease volume</source>
        <translation>Vähennä äänenvoimakkuutta</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="25"/>
        <source>Mute</source>
        <translation>Vaimenna</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="26"/>
        <source>Seek forward</source>
        <translation>Kelaa eteenpäin</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="27"/>
        <source>Seek backward</source>
        <translation>Kelaa taaksepäin</translation>
    </message>
</context>
<context>
    <name>HomeView</name>
    <message>
        <location filename="src/homeview.cpp" line="33"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="35"/>
        <source>Find videos and channels by keyword</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="40"/>
        <source>Browse</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="42"/>
        <source>Browse videos by category</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.h" line="22"/>
        <source>Make yourself comfortable</source>
        <translation>Tee olosi kotoisaksi</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="src/loadingwidget.cpp" line="90"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.cpp" line="213"/>
        <source>&amp;Stop</source>
        <translation>&amp;Pysäytä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="214"/>
        <source>Stop playback and go back to the search view</source>
        <translation>Pysäytä toisto ja palaa hakuruutuun</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="232"/>
        <source>S&amp;kip</source>
        <translation>&amp;Hyppää yli</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="233"/>
        <source>Skip to the next video</source>
        <translation>Siirry seuraavaan videoon</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="239"/>
        <location filename="src/mainwindow.cpp" line="989"/>
        <source>&amp;Pause</source>
        <translation>&amp;Keskeytä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="240"/>
        <location filename="src/mainwindow.cpp" line="990"/>
        <source>Pause playback</source>
        <translation>Keskeytä toisto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="246"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Koko näytön tila</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Go full screen</source>
        <translation>Siirry koko näytön tilaan</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="263"/>
        <source>Hide the playlist and the toolbar</source>
        <translation>Piilota soittolista sekä työkalupalkki</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="276"/>
        <source>Go to the YouTube video page and pause playback</source>
        <translation>Keskeytä toisto ja mene videon YouTube-sivulle</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="283"/>
        <source>Copy the current video YouTube link to the clipboard</source>
        <translation>Kopioi nykyisen videon YouTube-linkki leikepöydälle</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="290"/>
        <source>Copy the current video stream URL to the clipboard</source>
        <translation>Kopioi nykyisen videovirran osoite leikepöydälle</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="297"/>
        <source>Find other video parts hopefully in the right order</source>
        <translation>Löydä videon muut osat toivottavasti oikeassa järjestyksessä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="303"/>
        <source>&amp;Remove</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="304"/>
        <source>Remove the selected videos from the playlist</source>
        <translation>Poista valitut videot toistolistalta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="310"/>
        <source>Move &amp;Up</source>
        <translation>Siirrä &amp;ylös</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="311"/>
        <source>Move up the selected videos in the playlist</source>
        <translation>Siirrä valitut videot ylemmäksi soittolistalla</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="317"/>
        <source>Move &amp;Down</source>
        <translation>Sirrä &amp;alas</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="318"/>
        <source>Move down the selected videos in the playlist</source>
        <translation>Siirrä valitut videot alemmaksi soittolistalla</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="329"/>
        <source>Clear the search history. Cannot be undone.</source>
        <translation>Tyhjennä hakuhistoria. Tätä toimintoa ei voi kumota.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Lopeta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="337"/>
        <source>Bye</source>
        <translation>Näkemiin</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="341"/>
        <source>&amp;Website</source>
        <translation>&amp;Nettisivusto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="343"/>
        <source>%1 on the Web</source>
        <translation>%1 netissä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="349"/>
        <source>Please support the continued development of %1</source>
        <translation>Tue %1n jatkokehitystä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="354"/>
        <source>&amp;About</source>
        <translation>&amp;Tietoja</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="356"/>
        <source>Info about %1</source>
        <translation>Tietoja %1sta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="364"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="383"/>
        <source>Mute volume</source>
        <translation>Vaimenna ääni</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="412"/>
        <location filename="src/mainwindow.cpp" line="1365"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Lataukset</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="413"/>
        <source>Show details about video downloads</source>
        <translation>Näytä tietoja latauksista</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="421"/>
        <source>&amp;Download</source>
        <translation>&amp;Lataa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="422"/>
        <source>Download the current video</source>
        <translation>Lataa nykyinen video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="441"/>
        <source>Share the current video using %1</source>
        <translation>Jaa nykyinen video palvelulla %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="458"/>
        <source>&amp;Email</source>
        <translation>&amp;Sähköposti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="459"/>
        <source>Email</source>
        <translation>Sähköposti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="463"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="473"/>
        <source>&amp;Float on Top</source>
        <translation>&amp;Pysy päällimmäisenä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="478"/>
        <source>&amp;Stop After This Video</source>
        <translation>Py&amp;säytä toisto tämän videon jälkeen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="485"/>
        <source>&amp;Report an Issue...</source>
        <translation>&amp;Ilmoita ongelmasta...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="489"/>
        <source>&amp;Refine Search...</source>
        <translation>&amp;Määritä haku uudelleen...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="501"/>
        <source>More...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="504"/>
        <source>&amp;Related Videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="506"/>
        <source>Watch videos related to the current one</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="544"/>
        <source>&amp;Application</source>
        <translation>&amp;Sovellus</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="515"/>
        <source>Buy %1...</source>
        <translation>Osta %1...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="558"/>
        <source>&amp;Playback</source>
        <translation>&amp;Toisto</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="572"/>
        <source>&amp;Playlist</source>
        <translation>S&amp;oittolista</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="581"/>
        <source>&amp;Video</source>
        <translation>&amp;Video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="594"/>
        <source>&amp;View</source>
        <translation>&amp;Näytä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="601"/>
        <source>&amp;Share</source>
        <translation>&amp;Jaa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Ohje</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="690"/>
        <source>Press %1 to raise the volume, %2 to lower it</source>
        <translation>Paina näppäinyhdistelmää %1 nostaaksesi tai %2 laskeaksesi äänenvoimakkuutta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="889"/>
        <location filename="src/mainwindow.cpp" line="895"/>
        <source>Opening %1</source>
        <translation>Avataan kohdetta %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="936"/>
        <source>Do you want to exit %1 with a download in progress?</source>
        <translation>Haluatko, että %1 sulkeutuu vaikka lataus on kesken?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="937"/>
        <source>If you close %1 now, this download will be cancelled.</source>
        <translation>Jos suljet %1n nyt niin tämä lataus keskeytetään.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="942"/>
        <source>Close and cancel download</source>
        <translation>Sulje ja peru lataus</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="943"/>
        <source>Wait for download to finish</source>
        <translation>Odota latauksen valmistumista</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1102"/>
        <source>Leave &amp;Full Screen</source>
        <translation> Poistu &amp;koko näytön tilasta</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1475"/>
        <source>%1 version %2 is now available.</source>
        <translation>%1 versio %2 on nyt saatavilla.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1479"/>
        <source>Remind me later</source>
        <translation>Muistuta myöhemmin</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1480"/>
        <source>Update</source>
        <translation>Päivitä</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="982"/>
        <source>Error: %1</source>
        <translation>Virhe: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="222"/>
        <source>P&amp;revious</source>
        <translation>E&amp;dellinen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="223"/>
        <source>Go back to the previous track</source>
        <translation>Siirry takaisin edelliseen kappaleeseen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="262"/>
        <source>&amp;Compact Mode</source>
        <translation>Kompakti &amp;tila</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="275"/>
        <source>Open the &amp;YouTube Page</source>
        <translation>Avaa &amp;YouTube-sivu</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="282"/>
        <source>Copy the YouTube &amp;Link</source>
        <translation>Kopioi YouTube-&amp;linkki</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="289"/>
        <source>Copy the Video Stream &amp;URL</source>
        <translation>Kopioi videovirran &amp;URL</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="296"/>
        <source>Find Video &amp;Parts</source>
        <translation>Etsi videon &amp;osat</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>&amp;Clear Recent Searches</source>
        <translation>Ty&amp;hjennä viimeisimmät haut</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="348"/>
        <source>Make a &amp;Donation</source>
        <translation>Tee &amp;lahjoitus</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="405"/>
        <source>&amp;Manually Start Playing</source>
        <translation>Aloita toisto &amp;manuaalisesti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="406"/>
        <source>Manually start playing videos</source>
        <translation>Aloittaa videoiden toiston manuaalisesti</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="728"/>
        <source>Choose your content location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1002"/>
        <source>&amp;Play</source>
        <translation>&amp;Toista</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1003"/>
        <source>Resume playback</source>
        <translation>Jatka toistoa</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1236"/>
        <source>Remaining time: %1</source>
        <translation>Aikaa jäljellä: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1294"/>
        <source>Volume at %1%</source>
        <translation>Äänentaso: %1%</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1300"/>
        <source>Volume is muted</source>
        <translation>Ääni on vaimennettu</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1303"/>
        <source>Volume is unmuted</source>
        <translation>Ääni ei ole vaimennettu</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1310"/>
        <source>Maximum video definition set to %1</source>
        <translation>Korkein videonlaatu on rajoitettu tarkkuuteen %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1351"/>
        <source>Your privacy is now safe</source>
        <translation>Yksityisyytesi on nyt turvattu</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1366"/>
        <source>Downloads complete</source>
        <translation>Lataukset ovat valmistuneet</translation>
    </message>
</context>
<context>
    <name>MediaView</name>
    <message>
        <location filename="src/mediaview.cpp" line="553"/>
        <source>You can now paste the YouTube link into another application</source>
        <translation>Voit nyt liittää YouTube-linkin johonkin toiseen sovellukseen</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="561"/>
        <source>You can now paste the video stream URL into another application</source>
        <translation>Voit nyt liittää videovirran osoitteen (URL) johonkin toiseen sovellukseen</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="562"/>
        <source>The link will be valid only for a limited time.</source>
        <translation>Osoite on käytössä vain rajoitetun ajan.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="636"/>
        <source>This is just the demo version of %1.</source>
        <translation>Tämä on vain %1n kokeiluversio.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="637"/>
        <source>It allows you to test the application and see if it works for you.</source>
        <translation>Voit kokeilla ohjelmaa nähdäksesi, toimiiko se.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="667"/>
        <source>Continue</source>
        <translation>Jatka</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="756"/>
        <source>of</source>
        <comment>Used in video parts, as in '2 of 3'</comment>
        <translation>/</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="768"/>
        <source>part</source>
        <comment>This is for video parts, as in 'Cool video - part 1'</comment>
        <translation>osa</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="770"/>
        <source>episode</source>
        <comment>This is for video parts, as in 'Cool series - episode 1'</comment>
        <translation>jakso</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="847"/>
        <source>Sent from %1</source>
        <translation>Lähetetty palvelusta %1</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="644"/>
        <source>Get the full version</source>
        <translation>Hanki täysi versio</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="681"/>
        <source>Downloading %1</source>
        <translation>Ladataan %1ta/tä</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="21"/>
        <source>A new version of %1 is available!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="28"/>
        <source>%1 %2 is now available. You have %3.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="33"/>
        <source>Would you like to download it now?</source>
        <translation>Haluatko ladata sen nyt?</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="39"/>
        <source>Skip This Version</source>
        <translation>Ohita tämä versio</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="43"/>
        <source>Remind Me Later</source>
        <translation>Muistuta myöhemmin</translation>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="47"/>
        <source>Install Update</source>
        <translation>Asenna päivitys</translation>
    </message>
</context>
<context>
    <name>PasteLineEdit</name>
    <message>
        <location filename="local/src/pastelineedit.cpp" line="6"/>
        <source>Paste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="159"/>
        <source>%1 views</source>
        <translation>Katsottu %1 kertaa</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="285"/>
        <source>%1 of %2 (%3) — %4</source>
        <translation>%1 / %2 (%3) — %4</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="292"/>
        <source>Preparing</source>
        <translation>Valmistellaan</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="294"/>
        <source>Failed</source>
        <translation>Epäonnistui</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="296"/>
        <source>Completed</source>
        <translation>Valmis</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="298"/>
        <source>Stopped</source>
        <translation>Pysäytetty</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="334"/>
        <source>Stop downloading</source>
        <translation>Pysäytä lataus</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="344"/>
        <source>Show in %1</source>
        <translation>Avaa %1ssa/ssä</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="346"/>
        <source>Open parent folder</source>
        <translation>Avaa yläkansio</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="355"/>
        <source>Restart downloading</source>
        <translation>Käynnistä lataus uudelleen</translation>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="src/playlistmodel.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Etsitään...</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="53"/>
        <source>Show %1 More</source>
        <translation>Näytä %1 lisää</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="54"/>
        <source>No videos</source>
        <translation>Ei videoita</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="55"/>
        <source>No more videos</source>
        <translation>Ei enempää videoita</translation>
    </message>
</context>
<context>
    <name>RefineSearchWidget</name>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="31"/>
        <source>Sort by</source>
        <translation>Järjestysperuste</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="35"/>
        <source>Relevance</source>
        <translation>Olennaisuus</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="36"/>
        <location filename="src/refinesearchwidget.cpp" line="52"/>
        <source>Date</source>
        <translation>Päiväys</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="37"/>
        <source>View Count</source>
        <translation>Katselukerrat</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="38"/>
        <source>Rating</source>
        <translation>Arvostelu</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="56"/>
        <source>Anytime</source>
        <translation>Milloin tahansa</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="57"/>
        <source>Today</source>
        <translation>Tänään</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="58"/>
        <source>7 Days</source>
        <translation>7 päivää</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="59"/>
        <source>30 Days</source>
        <translation>30 päivää</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="73"/>
        <source>Duration</source>
        <translation>Kesto</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="77"/>
        <location filename="src/refinesearchwidget.cpp" line="104"/>
        <source>All</source>
        <translation>Kaikki</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="78"/>
        <source>Short</source>
        <translation>Lyhyt</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="79"/>
        <source>Medium</source>
        <translation>Keskipituinen</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="80"/>
        <source>Long</source>
        <translation>Pitkä</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="83"/>
        <source>Less than 4 minutes</source>
        <translation>Vähemmän kuin 4 minuuttia</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="84"/>
        <source>Between 4 and 20 minutes</source>
        <translation>4-20 minuutin välillä</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="85"/>
        <source>Longer than 20 minutes</source>
        <translation>Pidempi kuin 20 minuuttia</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="100"/>
        <source>Quality</source>
        <translation>Laatu</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="105"/>
        <source>High Definition</source>
        <translation>Teräväpiirto</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="108"/>
        <source>720p or higher</source>
        <translation>720p tai tarkempi</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="122"/>
        <source>Done</source>
        <translation>Valmis</translation>
    </message>
</context>
<context>
    <name>RegionsView</name>
    <message>
        <location filename="src/regionsview.cpp" line="19"/>
        <source>Done</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="177"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="src/searchview.cpp" line="71"/>
        <source>Welcome to &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</source>
        <translation>Tervetuloa &lt;a href=&apos;%1&apos;&gt;%2en&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="50"/>
        <source>Get the full version</source>
        <translation>Hanki täysversio</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="85"/>
        <source>Enter</source>
        <extracomment>&quot;Enter&quot;, as in &quot;type&quot;. The whole phrase says: &quot;Enter a keyword to start watching videos&quot;</extracomment>
        <translation>Syötä</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="90"/>
        <source>a keyword</source>
        <translation>hakusana</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="91"/>
        <source>a channel</source>
        <translation>kanava</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="96"/>
        <source>to start watching videos.</source>
        <translation>aloittaaksesi videoiden katselu.</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="120"/>
        <source>Watch</source>
        <translation>Katso</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="138"/>
        <source>Recent keywords</source>
        <translation>Viimeisimmät hakusanat</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="151"/>
        <source>Recent channels</source>
        <translation>Viimeisimmät kanavat</translation>
    </message>
</context>
<context>
    <name>SidebarHeader</name>
    <message>
        <location filename="src/sidebarheader.cpp" line="16"/>
        <location filename="src/sidebarheader.cpp" line="23"/>
        <source>&amp;Back</source>
        <translation>&amp;Takaisin</translation>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="57"/>
        <source>Forward to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="70"/>
        <source>Back to %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <location filename="src/sidebarwidget.cpp" line="52"/>
        <source>Refine Search</source>
        <translation>Määritä haku uudelleen</translation>
    </message>
    <message>
        <location filename="src/sidebarwidget.cpp" line="148"/>
        <source>Did you mean: %1</source>
        <translation>Tarkoititko: %1</translation>
    </message>
</context>
<context>
    <name>StandardFeedsView</name>
    <message>
        <location filename="src/standardfeedsview.cpp" line="85"/>
        <source>Most Popular</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="86"/>
        <source>Featured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="87"/>
        <source>Most Shared</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="88"/>
        <source>Most Discussed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="89"/>
        <source>Top Rated</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Video</name>
    <message>
        <location filename="src/video.cpp" line="213"/>
        <source>Cannot get video stream for %1</source>
        <translation>Videostriimiä ei saada kohteelle %1</translation>
    </message>
    <message>
        <location filename="src/video.cpp" line="231"/>
        <source>Network error: %1 for %2</source>
        <translation>Verkkovirhe: &quot;%1&quot; &quot;%2&quot;lle</translation>
    </message>
</context>
<context>
    <name>YTRegions</name>
    <message>
        <location filename="src/ytregions.cpp" line="8"/>
        <source>Algeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="9"/>
        <source>Argentina</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="10"/>
        <source>Australia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="11"/>
        <source>Belgium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="12"/>
        <source>Brazil</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="13"/>
        <source>Canada</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="14"/>
        <source>Chile</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="15"/>
        <source>Colombia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="16"/>
        <source>Czech Republic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="17"/>
        <source>Egypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="18"/>
        <source>France</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="19"/>
        <source>Germany</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="20"/>
        <source>Ghana</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="21"/>
        <source>Greece</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="22"/>
        <source>Hong Kong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="23"/>
        <source>Hungary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="24"/>
        <source>India</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="25"/>
        <source>Indonesia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="26"/>
        <source>Ireland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="27"/>
        <source>Israel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="28"/>
        <source>Italy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="29"/>
        <source>Japan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="30"/>
        <source>Jordan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="31"/>
        <source>Kenya</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="32"/>
        <source>Malaysia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="33"/>
        <source>Mexico</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="34"/>
        <source>Morocco</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="35"/>
        <source>Netherlands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="36"/>
        <source>New Zealand</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="37"/>
        <source>Nigeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="38"/>
        <source>Peru</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="39"/>
        <source>Philippines</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="40"/>
        <source>Poland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="41"/>
        <source>Russia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="42"/>
        <source>Saudi Arabia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="43"/>
        <source>Singapore</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="44"/>
        <source>South Africa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="45"/>
        <source>South Korea</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="46"/>
        <source>Spain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="47"/>
        <source>Sweden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="48"/>
        <source>Taiwan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="49"/>
        <source>Tunisia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="50"/>
        <source>Turkey</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="51"/>
        <source>Uganda</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="52"/>
        <source>United Arab Emirates</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="53"/>
        <source>United Kingdom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="54"/>
        <source>Yemen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="128"/>
        <source>Worldwide</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>