<?xml version="1.0" ?><!DOCTYPE TS><TS language="de_DE" version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutView</name>
    <message>
        <location filename="src/aboutview.cpp" line="32"/>
        <source>There&apos;s life outside the browser!</source>
        <translation>Es gibt ein Leben außerhalb des Browsers!</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="33"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="38"/>
        <source>Licensed to: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="42"/>
        <source>%1 is Free Software but its development takes precious time.</source>
        <translation>%1 ist freie Software, aber die Entwicklung kostet wertvolle Zeit.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="43"/>
        <source>Please &lt;a href=&apos;%1&apos;&gt;donate&lt;/a&gt; to support the continued development of %2.</source>
        <translation>Bitte &lt;a href=&apos;%1&apos;&gt;spende&lt;/a&gt;, um die ständige Entwicklung von %2 zu unterstützen.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="47"/>
        <source>You may want to try my other apps as well:</source>
        <translation>Vielleicht möchten Sie auch meine anderen Apps ausprobieren:</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="50"/>
        <source>%1, a YouTube music player</source>
        <translation>%1, ein YouTube-Musik-Player</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="54"/>
        <source>%1, a music player</source>
        <translation>%1, ein Musik-Player</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="60"/>
        <source>Translate %1 to your native language using %2</source>
        <translation>Übersetzen Sie %1 in Ihre Muttersprache mit %2</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="65"/>
        <source>Icon designed by %1.</source>
        <translation>Symbol-Entwurf durch %1.</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="69"/>
        <source>Released under the &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</source>
        <translation>Veröffentlicht unter der &lt;a href=&apos;%1&apos;&gt;GNU General Public License&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="src/aboutview.cpp" line="84"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="17"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="src/aboutview.h" line="19"/>
        <source>What you always wanted to know about %1 and never dared to ask</source>
        <translation>Was Du schon immer über %1 wissen wolltest, aber nie zu fragen wagtest</translation>
    </message>
</context>
<context>
    <name>ActivationDialog</name>
    <message>
        <location filename="local/src/activationdialog.cpp" line="17"/>
        <source>Enter your License Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="29"/>
        <source>&amp;Email:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationdialog.cpp" line="35"/>
        <source>&amp;Code:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ActivationView</name>
    <message>
        <location filename="local/src/activationview.cpp" line="47"/>
        <source>Please license %1</source>
        <translation>Bitte lizenzieren Sie %1</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="51"/>
        <source>This demo has expired.</source>
        <translation>Diese Demo ist abgelaufen.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="53"/>
        <source>The full version allows you to download videos longer than %1 minutes and to watch videos without interruptions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="55"/>
        <source>Without a license, the application will expire in %1 days.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="57"/>
        <source>By purchasing the full version, you will also support the hard work I put into creating %1.</source>
        <translation>Mit dem Kauf der Vollversion unterstützen Sie auch die harte Arbeit, die ich in die Erstellung von %1 gesteckt habe.</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="70"/>
        <source>Use Demo</source>
        <translation>Demo verwenden</translation>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="77"/>
        <source>Enter License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/activationview.cpp" line="85"/>
        <source>Buy License</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="56"/>
        <source>Clear</source>
        <translation>Säubern</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="src/downloaditem.cpp" line="322"/>
        <source>bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="325"/>
        <source>KB</source>
        <translation>kB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="328"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="337"/>
        <source>bytes/sec</source>
        <translation>Bytes/s</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="340"/>
        <source>KB/sec</source>
        <translation>kB/s</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="343"/>
        <source>MB/sec</source>
        <translation>MB/s</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="349"/>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="352"/>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="src/downloaditem.cpp" line="355"/>
        <source>%4 %5 remaining</source>
        <translation>%4 %5 verbleibend</translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="55"/>
        <source>This is just the demo version of %1.</source>
        <translation>Dies ist nur die Demoversion von %1.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="57"/>
        <source>It can only download videos shorter than %1 minutes so you can test the download functionality.</source>
        <translation>Sie kann nur Videos, die kürzer als %1 Minuten sind, herunterladen, damit die Download-Funktionalität getestet werden kann.</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="63"/>
        <source>Continue</source>
        <translation>Fortfahren</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="64"/>
        <source>Get the full version</source>
        <translation>Die Vollversion holen</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="150"/>
        <source>%1 downloaded in %2</source>
        <translation>%1 heruntergeladen nach %2</translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="153"/>
        <source>Download finished</source>
        <translation>Herunterladen abgeschlossen</translation>
    </message>
    <message numerus="yes">
        <location filename="src/downloadmanager.cpp" line="159"/>
        <source>%n Download(s)</source>
        <translation><numerusform>%n Download</numerusform><numerusform>%n Downloads</numerusform></translation>
    </message>
</context>
<context>
    <name>DownloadSettings</name>
    <message>
        <location filename="src/downloadsettings.cpp" line="15"/>
        <source>Change location...</source>
        <translation>Speicherort ändern…</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="47"/>
        <source>Choose the download location</source>
        <translation>Wähle den Speicherort</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="59"/>
        <source>Download location changed.</source>
        <translation>Speicherort wurde verändert.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="61"/>
        <source>Current downloads will still go in the previous location.</source>
        <translation>Aktuelle Downloads werden immernoch im alten Speicherort gespeichert.</translation>
    </message>
    <message>
        <location filename="src/downloadsettings.cpp" line="72"/>
        <source>Downloading to: %1</source>
        <translation>Lade herunter nach: %1</translation>
    </message>
</context>
<context>
    <name>DownloadView</name>
    <message>
        <location filename="src/downloadview.cpp" line="18"/>
        <location filename="src/downloadview.h" line="22"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="60"/>
        <source>Downloading update...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GlobalShortcuts</name>
    <message>
        <location filename="src/globalshortcuts.cpp" line="16"/>
        <source>Play</source>
        <translation>Abspielen</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="17"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="18"/>
        <source>Play/Pause</source>
        <translation>Abspielen/Pause</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="19"/>
        <source>Stop</source>
        <translation>Halt</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="20"/>
        <source>Stop playing after current track</source>
        <translation>Nachdem aktueller Titel abgespielt wurde halten</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="21"/>
        <source>Next track</source>
        <translation>Nächster Titel</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="22"/>
        <source>Previous track</source>
        <translation>Vorheriger Titel</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="23"/>
        <source>Increase volume</source>
        <translation>Lautstärke erhöhen</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="24"/>
        <source>Decrease volume</source>
        <translation>Lautstärke verringern</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="25"/>
        <source>Mute</source>
        <translation>Stummschalten</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="26"/>
        <source>Seek forward</source>
        <translation>Vorwärts spulen</translation>
    </message>
    <message>
        <location filename="src/globalshortcuts.cpp" line="27"/>
        <source>Seek backward</source>
        <translation>Rückwärts spulen</translation>
    </message>
</context>
<context>
    <name>HomeView</name>
    <message>
        <location filename="src/homeview.cpp" line="33"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="35"/>
        <source>Find videos and channels by keyword</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="40"/>
        <source>Browse</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.cpp" line="42"/>
        <source>Browse videos by category</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/homeview.h" line="22"/>
        <source>Make yourself comfortable</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="src/loadingwidget.cpp" line="90"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.cpp" line="213"/>
        <source>&amp;Stop</source>
        <translation>&amp;Halt</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="214"/>
        <source>Stop playback and go back to the search view</source>
        <translation>Wiedergabe anhalten und zur Suchansicht zurückkehren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="232"/>
        <source>S&amp;kip</source>
        <translation>Ü&amp;berspringen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="233"/>
        <source>Skip to the next video</source>
        <translation>Zum nächsten Video springen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="239"/>
        <location filename="src/mainwindow.cpp" line="989"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="240"/>
        <location filename="src/mainwindow.cpp" line="990"/>
        <source>Pause playback</source>
        <translation>Wiedergabe pausieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="246"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Vollbildmodus</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Go full screen</source>
        <translation>Vollbildmodus aktivieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="263"/>
        <source>Hide the playlist and the toolbar</source>
        <translation>Verstecke Abspielliste und Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="276"/>
        <source>Go to the YouTube video page and pause playback</source>
        <translation>Gehe zur YouTube-Video-Seite und pausiere die Wiedergabe</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="283"/>
        <source>Copy the current video YouTube link to the clipboard</source>
        <translation>YouTube-Link in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="290"/>
        <source>Copy the current video stream URL to the clipboard</source>
        <translation>Video-URL in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="297"/>
        <source>Find other video parts hopefully in the right order</source>
        <translation>Findet andere Teile des Videos, hoffentlich in der richtigen Reihenfolge</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="303"/>
        <source>&amp;Remove</source>
        <translation>Entfe&amp;rnen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="304"/>
        <source>Remove the selected videos from the playlist</source>
        <translation>Entferne das ausgewählte Video aus der Abspielliste</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="310"/>
        <source>Move &amp;Up</source>
        <translation>Bewege &amp;hinauf</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="311"/>
        <source>Move up the selected videos in the playlist</source>
        <translation>Bewege das ausgewählte Video in der Abspielliste hinauf</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="317"/>
        <source>Move &amp;Down</source>
        <translation>Bewege hin&amp;ab</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="318"/>
        <source>Move down the selected videos in the playlist</source>
        <translation>Bewege das ausgewählte Video in der Abspielliste hinunter</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="329"/>
        <source>Clear the search history. Cannot be undone.</source>
        <translation>Such-Verlauf leeren. Kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Verlassen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="337"/>
        <source>Bye</source>
        <translation>Tschüss</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="341"/>
        <source>&amp;Website</source>
        <translation>&amp;Webseite</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="343"/>
        <source>%1 on the Web</source>
        <translation>%1 im Web</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="349"/>
        <source>Please support the continued development of %1</source>
        <translation>Bitte unterstützen Sie die weitere Entwicklung von %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="354"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="356"/>
        <source>Info about %1</source>
        <translation>Informationen über %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="364"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="383"/>
        <source>Mute volume</source>
        <translation>Stummschalten</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="412"/>
        <location filename="src/mainwindow.cpp" line="1365"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Downloads</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="413"/>
        <source>Show details about video downloads</source>
        <translation>Details über Video-Downloads anzeigen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="421"/>
        <source>&amp;Download</source>
        <translation>H&amp;erunterladen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="422"/>
        <source>Download the current video</source>
        <translation>Das aktuelle Video herunterladen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="441"/>
        <source>Share the current video using %1</source>
        <translation>Teilen Sie das aktuellen Video mit %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="458"/>
        <source>&amp;Email</source>
        <translation>&amp;E-Mail</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="459"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="463"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="473"/>
        <source>&amp;Float on Top</source>
        <translation>Im Vordergrund &amp;bleiben</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="478"/>
        <source>&amp;Stop After This Video</source>
        <translation>Nach diesem Video &amp;anhalten</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="485"/>
        <source>&amp;Report an Issue...</source>
        <translation>&amp;Ein Problem melden ...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="489"/>
        <source>&amp;Refine Search...</source>
        <translation>&amp;Suche verfeinern ...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="501"/>
        <source>More...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="504"/>
        <source>&amp;Related Videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="506"/>
        <source>Watch videos related to the current one</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="544"/>
        <source>&amp;Application</source>
        <translation>&amp;Anwendung</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="515"/>
        <source>Buy %1...</source>
        <translation>%1 kaufen...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="558"/>
        <source>&amp;Playback</source>
        <translation>&amp;Wiedergabe</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="572"/>
        <source>&amp;Playlist</source>
        <translation>A&amp;bspielliste</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="581"/>
        <source>&amp;Video</source>
        <translation>&amp;Video</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="594"/>
        <source>&amp;View</source>
        <translation>&amp;Ansehen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="601"/>
        <source>&amp;Share</source>
        <translation>&amp;Teilen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="690"/>
        <source>Press %1 to raise the volume, %2 to lower it</source>
        <translation>%1 drücken, um die Lautstärke zu erhöhen, %2 um sie zu verringern</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="889"/>
        <location filename="src/mainwindow.cpp" line="895"/>
        <source>Opening %1</source>
        <translation>%1 öffnen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="936"/>
        <source>Do you want to exit %1 with a download in progress?</source>
        <translation>Möchten Sie %1 mit einem aktiven Download beenden?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="937"/>
        <source>If you close %1 now, this download will be cancelled.</source>
        <translation>Wenn Sie %1 jetzt beenden, wird der Download abgebrochen.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="942"/>
        <source>Close and cancel download</source>
        <translation>Beenden und Download abbrechen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="943"/>
        <source>Wait for download to finish</source>
        <translation>Auf Abschluß des Downloads warten</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1102"/>
        <source>Leave &amp;Full Screen</source>
        <translation>&amp;Vollbild verlassen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1475"/>
        <source>%1 version %2 is now available.</source>
        <translation>%1 Version %2 ist jetzt verfügbar.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1479"/>
        <source>Remind me later</source>
        <translation>Erinnere mich später</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1480"/>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="982"/>
        <source>Error: %1</source>
        <translation>Fehler: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="222"/>
        <source>P&amp;revious</source>
        <translation>V&amp;origes</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="223"/>
        <source>Go back to the previous track</source>
        <translation>Zum vorherigen Titel zurückgehen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="262"/>
        <source>&amp;Compact Mode</source>
        <translation>&amp;Kompakt-Modus</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="275"/>
        <source>Open the &amp;YouTube Page</source>
        <translation>&amp;YouTube-Seite öffnen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="282"/>
        <source>Copy the YouTube &amp;Link</source>
        <translation>YouTube &amp;Link kopieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="289"/>
        <source>Copy the Video Stream &amp;URL</source>
        <translation>Video-Stream &amp;URL kopieren</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="296"/>
        <source>Find Video &amp;Parts</source>
        <translation>Suche Video &amp;Teile</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>&amp;Clear Recent Searches</source>
        <translation>Vorherige Suchbegriffe löschen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="348"/>
        <source>Make a &amp;Donation</source>
        <translation>&amp;Spenden</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="405"/>
        <source>&amp;Manually Start Playing</source>
        <translation>&amp;Manuelles Starten der Wiedergabe</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="406"/>
        <source>Manually start playing videos</source>
        <translation>Manuelles Starten der Video-Wiedergabe</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="728"/>
        <source>Choose your content location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1002"/>
        <source>&amp;Play</source>
        <translation>&amp;Abspielen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1003"/>
        <source>Resume playback</source>
        <translation>Wiedergabe fortsetzen</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1236"/>
        <source>Remaining time: %1</source>
        <translation>Verbleibende Zeit: %1</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1294"/>
        <source>Volume at %1%</source>
        <translation>Lautstärke %1%</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1300"/>
        <source>Volume is muted</source>
        <translation>Ton ist stummgeschaltet</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1303"/>
        <source>Volume is unmuted</source>
        <translation>Ton ist angeschaltet</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1310"/>
        <source>Maximum video definition set to %1</source>
        <translation>Maximale Video-Auflösung wurde auf %1 gesetzt</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1351"/>
        <source>Your privacy is now safe</source>
        <translation>Ihre Privatsphäre ist nun geschützt</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="1366"/>
        <source>Downloads complete</source>
        <translation>Downloads heruntergeladen</translation>
    </message>
</context>
<context>
    <name>MediaView</name>
    <message>
        <location filename="src/mediaview.cpp" line="553"/>
        <source>You can now paste the YouTube link into another application</source>
        <translation>Du kannst den YouTube-Link nun in einer anderen Anwendung einfügen</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="561"/>
        <source>You can now paste the video stream URL into another application</source>
        <translation>Du kannst die Video-URL nun in einer anderen Anwendung einfügen</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="562"/>
        <source>The link will be valid only for a limited time.</source>
        <translation>Der Link wird nur eine beschränkte Zeit gültig sein.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="636"/>
        <source>This is just the demo version of %1.</source>
        <translation>Dies ist nur eine Demoversion von %1.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="637"/>
        <source>It allows you to test the application and see if it works for you.</source>
        <translation>Sie erlaubt Ihnen die Anwendung zu testen und zu schauen ob sie bei Ihnen läuft.</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="667"/>
        <source>Continue</source>
        <translation>Fortfahren</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="756"/>
        <source>of</source>
        <comment>Used in video parts, as in '2 of 3'</comment>
        <translation>von </translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="768"/>
        <source>part</source>
        <comment>This is for video parts, as in 'Cool video - part 1'</comment>
        <translation>Teil</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="770"/>
        <source>episode</source>
        <comment>This is for video parts, as in 'Cool series - episode 1'</comment>
        <translation>Episode</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="847"/>
        <source>Sent from %1</source>
        <translation>Gesendet von %1</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="644"/>
        <source>Get the full version</source>
        <translation>Die Vollversion holen</translation>
    </message>
    <message>
        <location filename="src/mediaview.cpp" line="681"/>
        <source>Downloading %1</source>
        <translation>%1 herunterladen</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="local/src/updatedialog.cpp" line="21"/>
        <source>A new version of %1 is available!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="28"/>
        <source>%1 %2 is now available. You have %3.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="33"/>
        <source>Would you like to download it now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="39"/>
        <source>Skip This Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="43"/>
        <source>Remind Me Later</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="local/src/updatedialog.cpp" line="47"/>
        <source>Install Update</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PasteLineEdit</name>
    <message>
        <location filename="local/src/pastelineedit.cpp" line="6"/>
        <source>Paste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="159"/>
        <source>%1 views</source>
        <translation>%1 mal betrachtet</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="285"/>
        <source>%1 of %2 (%3) — %4</source>
        <translation>%1 von %2 (%3) – %4</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="292"/>
        <source>Preparing</source>
        <translation>Bereite vor</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="294"/>
        <source>Failed</source>
        <translation>Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="296"/>
        <source>Completed</source>
        <translation>Fertiggestellt</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="298"/>
        <source>Stopped</source>
        <translation>Gestoppt</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="334"/>
        <source>Stop downloading</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="344"/>
        <source>Show in %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="346"/>
        <source>Open parent folder</source>
        <translation>Eltern-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="src/playlistitemdelegate.cpp" line="355"/>
        <source>Restart downloading</source>
        <translation>Herunterladen neustarten</translation>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="src/playlistmodel.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Suche...</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="53"/>
        <source>Show %1 More</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="54"/>
        <source>No videos</source>
        <translation>Keine Videos</translation>
    </message>
    <message>
        <location filename="src/playlistmodel.cpp" line="55"/>
        <source>No more videos</source>
        <translation>Keine weiteren Videos</translation>
    </message>
</context>
<context>
    <name>RefineSearchWidget</name>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="31"/>
        <source>Sort by</source>
        <translation>Sortieren nach</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="35"/>
        <source>Relevance</source>
        <translation>Relevanz</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="36"/>
        <location filename="src/refinesearchwidget.cpp" line="52"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="37"/>
        <source>View Count</source>
        <translation>Anzahl anzeigen</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="38"/>
        <source>Rating</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="56"/>
        <source>Anytime</source>
        <translation>Alle Längen</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="57"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="58"/>
        <source>7 Days</source>
        <translation>7 Tage</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="59"/>
        <source>30 Days</source>
        <translation>30 Tage</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="73"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="77"/>
        <location filename="src/refinesearchwidget.cpp" line="104"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="78"/>
        <source>Short</source>
        <translation>Kurz</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="79"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="80"/>
        <source>Long</source>
        <translation>Lang</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="83"/>
        <source>Less than 4 minutes</source>
        <translation>Weniger als 4 Minuten</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="84"/>
        <source>Between 4 and 20 minutes</source>
        <translation>Zwischen 4 und 20 Minuten</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="85"/>
        <source>Longer than 20 minutes</source>
        <translation>Länger als 20 Minuten</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="100"/>
        <source>Quality</source>
        <translation>Qualität</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="105"/>
        <source>High Definition</source>
        <translation>High Definition</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="108"/>
        <source>720p or higher</source>
        <translation>720p oder höher</translation>
    </message>
    <message>
        <location filename="src/refinesearchwidget.cpp" line="122"/>
        <source>Done</source>
        <translation>Erledigt</translation>
    </message>
</context>
<context>
    <name>RegionsView</name>
    <message>
        <location filename="src/regionsview.cpp" line="19"/>
        <source>Done</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="src/searchlineedit.cpp" line="177"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="src/searchview.cpp" line="71"/>
        <source>Welcome to &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</source>
        <translation>Willkommen bei &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="50"/>
        <source>Get the full version</source>
        <translation>Die Vollversion holen</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="85"/>
        <source>Enter</source>
        <extracomment>&quot;Enter&quot;, as in &quot;type&quot;. The whole phrase says: &quot;Enter a keyword to start watching videos&quot;</extracomment>
        <translation>Eingeben</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="90"/>
        <source>a keyword</source>
        <translation>ein Schlüsselwort</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="91"/>
        <source>a channel</source>
        <translation>ein Kanal</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="96"/>
        <source>to start watching videos.</source>
        <translation>um die Wiedergabe zu starten.</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="120"/>
        <source>Watch</source>
        <translation>Anschauen</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="138"/>
        <source>Recent keywords</source>
        <translation>Aktuelle Schlüsselwörter</translation>
    </message>
    <message>
        <location filename="src/searchview.cpp" line="151"/>
        <source>Recent channels</source>
        <translation>Aktuelle Kanäle</translation>
    </message>
</context>
<context>
    <name>SidebarHeader</name>
    <message>
        <location filename="src/sidebarheader.cpp" line="16"/>
        <location filename="src/sidebarheader.cpp" line="23"/>
        <source>&amp;Back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="57"/>
        <source>Forward to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/sidebarheader.cpp" line="70"/>
        <source>Back to %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <location filename="src/sidebarwidget.cpp" line="52"/>
        <source>Refine Search</source>
        <translation>Suche verfeinern</translation>
    </message>
    <message>
        <location filename="src/sidebarwidget.cpp" line="148"/>
        <source>Did you mean: %1</source>
        <translation>Haben Sie gemeint: %1</translation>
    </message>
</context>
<context>
    <name>StandardFeedsView</name>
    <message>
        <location filename="src/standardfeedsview.cpp" line="85"/>
        <source>Most Popular</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="86"/>
        <source>Featured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="87"/>
        <source>Most Shared</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="88"/>
        <source>Most Discussed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/standardfeedsview.cpp" line="89"/>
        <source>Top Rated</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Video</name>
    <message>
        <location filename="src/video.cpp" line="213"/>
        <source>Cannot get video stream for %1</source>
        <translation>Videostream für %1 konnte nicht öffnen werden</translation>
    </message>
    <message>
        <location filename="src/video.cpp" line="231"/>
        <source>Network error: %1 for %2</source>
        <translation>Netzwerk-Fehler: %1 für %2</translation>
    </message>
</context>
<context>
    <name>YTRegions</name>
    <message>
        <location filename="src/ytregions.cpp" line="8"/>
        <source>Algeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="9"/>
        <source>Argentina</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="10"/>
        <source>Australia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="11"/>
        <source>Belgium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="12"/>
        <source>Brazil</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="13"/>
        <source>Canada</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="14"/>
        <source>Chile</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="15"/>
        <source>Colombia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="16"/>
        <source>Czech Republic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="17"/>
        <source>Egypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="18"/>
        <source>France</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="19"/>
        <source>Germany</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="20"/>
        <source>Ghana</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="21"/>
        <source>Greece</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="22"/>
        <source>Hong Kong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="23"/>
        <source>Hungary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="24"/>
        <source>India</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="25"/>
        <source>Indonesia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="26"/>
        <source>Ireland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="27"/>
        <source>Israel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="28"/>
        <source>Italy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="29"/>
        <source>Japan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="30"/>
        <source>Jordan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="31"/>
        <source>Kenya</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="32"/>
        <source>Malaysia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="33"/>
        <source>Mexico</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="34"/>
        <source>Morocco</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="35"/>
        <source>Netherlands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="36"/>
        <source>New Zealand</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="37"/>
        <source>Nigeria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="38"/>
        <source>Peru</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="39"/>
        <source>Philippines</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="40"/>
        <source>Poland</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="41"/>
        <source>Russia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="42"/>
        <source>Saudi Arabia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="43"/>
        <source>Singapore</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="44"/>
        <source>South Africa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="45"/>
        <source>South Korea</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="46"/>
        <source>Spain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="47"/>
        <source>Sweden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="48"/>
        <source>Taiwan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="49"/>
        <source>Tunisia</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="50"/>
        <source>Turkey</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="51"/>
        <source>Uganda</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="52"/>
        <source>United Arab Emirates</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="53"/>
        <source>United Kingdom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="54"/>
        <source>Yemen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="src/ytregions.cpp" line="128"/>
        <source>Worldwide</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>